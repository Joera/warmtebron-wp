<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_url',
        'title' => 'Externe link',
        'fields' => array (
            array(
                'key' => 'acf_partner_url',
                'label' => 'Externe link',
                'name' => 'partner_url',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'partner',
                    'order_no' => 0,
                    'group_no' => 1,
                )
            )
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'metabox',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

?>