<?php

add_action( 'init', 'register_partner_url' );

function register_partner_url() {
    register_rest_field( array('partner'),
        'partner_url',
        array(
            'get_callback'    => 'get_partner_url',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_partner_url( $object, $field_name, $request )
{
    return get_field('partner_url',$object['id']);
}
