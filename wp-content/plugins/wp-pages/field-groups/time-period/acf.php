<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_extra-velden-post',
        'title' => 'Extra velden post',
        'fields' => array (
            array (
                'key' => 'dates_start777',
                'label' => 'Startdatum',
                'name' => 'start-date',
                'type' => 'date_picker',
                'first_day' => 1,
                'return_format' => 'd/m/Y',
                'display_format' => 'd/m/Y',
                'save_format' => 'yy-mm-dd',
            ),
            array (
                'key' => 'dates_end888',
                'label' => 'Einddatum',
                'name' => 'end-date',
                'type' => 'date_picker',
                'first_day' => 1,
                'return_format' => 'd/m/Y',
                'display_format' => 'd/m/Y',
                'save_format' => 'yy-mm-dd',
            ),
        ),

        'location' => configToLocations('time_period'),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

?>