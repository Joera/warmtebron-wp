<?php

add_action( 'init', 'register_time_period' );

function register_time_period() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->time_period,
        'time_period',
        array(
            'get_callback'    => 'get_time_period',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_time_period( $object, $field_name, $request ) {

    $time_period = new stdClass();
    $time_period->start = get_field('start-date',$object['id']);
    $time_period->end = get_field('end-date',$object['id']);

    return $time_period;
}