<?php

add_action( 'init', 'register_subtitle' );

function register_subtitle() {
    register_rest_field( array('page'),
        'subtitle',
        array(
            'get_callback'    => 'get_subtitle',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_subtitle( $object, $field_name, $request){
    return get_field('subtitle', $object['id']);
}
