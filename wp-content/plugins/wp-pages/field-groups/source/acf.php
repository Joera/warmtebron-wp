<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_source',
        'title' => 'Bron',
        'fields' => array (
            array(
                'key' => 'acf_source_url',
                'label' => 'Url',
                'name' => 'source_url',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                    'order_no' => 0,
                    'group_no' => 1,
                )
            )
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'metabox',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

?>