<?php

add_action( 'init', 'register_featured_content' );

function register_featured_content() {
    register_rest_field( array('page'),
        'featured_content',
        array(
            'get_callback'    => 'get_featured_content',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_featured_content( $object, $field_name, $request )
{
    $featured_content = [];

    if(have_rows('featured-content', $object['id'])):
        while ( have_rows('featured-content', $object['id']) ) : the_row();

            $acf_blog = get_sub_field('blog');
            $featured_post = get_post($acf_blog);
            $controller = new WP_REST_Posts_Controller($featured_post->post_type);
            $new_object = $controller->prepare_item_for_response( $featured_post, $request)->data;

            $new_object['url'] = get_permalink($featured_post->ID);
            $featured_content[] = $new_object;
        endwhile;
    endif;

    return $featured_content;
}
