<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_contact_person',
        'title' => 'Contactpersoon',
        'fields' => array (
            array(
                'key' => 'acf_contact_person_function',
                'label' => 'Functie',
                'name' => 'function',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'acf_contact_person_email',
                'label' => 'Email',
                'name' => 'email',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'acf_contact_person_phone',
                'label' => 'Telefoon',
                'name' => 'phone',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'acf_contact_person_introduction',
                'label' => 'Introductie',
                'name' => 'introduction',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            )
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'contact',
                    'order_no' => 4,
                    'group_no' => 4,
                )
            )
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'metabox',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 3,
        'position' => 'normal',
        'style' => 'metabox',
        'label_placement' => 'none',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
}

?>