<?php

add_action( 'init', 'register_contact_person' );

function register_contact_person() {
    register_rest_field( array('contact'),
        'contact',
        array(
            'get_callback'    => 'get_contact',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_contact( $object, $field_name, $request ) {

    $contact = new stdClass();
    $contact->function = get_field('acf_contact_person_function',$object['id']);
    $contact->email = get_field('acf_contact_person_email',$object['id']);
    $contact->phone = get_field('acf_contact_person_phone',$object['id']);
    $contact->introduction = get_field('acf_contact_person_introduction',$object['id']);

    return $contact;
}
