<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_calendar',
        'title' => 'Agendafuncties',
        'fields' => array (
//            array(
//                'key' => 'acf_calendar_recurrent',
//                'label' => 'Herhalend',
//                'name' => 'recurrent',
//                'type' => 'radio',
//                'choices' => array (
//                    'not' => 'Eenmalig',
//                    'weekly' => 'Wekelijks',
//                    'monthly' => 'Maandelijks'
//                ),
//                'default_value' => 'not',
//                'layout' => 'horizontal'
//            ),
            array(
                'key' => 'acf_calendar_recurring_dates',
                'label' => 'Herhaaldata',
                'name' => 'recurring_dates',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'row',
                'button_label' => 'Extra datum',
                'sub_fields' => array(
                    array(
                        'key' => 'acf_session_recurring_date',
                        'label' => 'Datum',
                        'name' => 'recurring_date',
                        'type' => 'date_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => 'half',
                            'id' => '',
                        ),
                        'display_format' => 'd-m-Y  ',
                        'return_format' => 'd-m-Y',
                        'first_day' => 1,
                    ),
                )
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'activity',
                    'order_no' => 4,
                    'group_no' => 4,
                )
            )
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'metabox',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 3,
        'position' => 'normal',
        'style' => 'metabox',
        'label_placement' => 'none',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
}

?>