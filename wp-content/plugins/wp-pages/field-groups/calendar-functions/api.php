<?php

add_action( 'init', 'register_calendar_functions' );

function register_calendar_functions() {
    register_rest_field( array('activity'),
        'calendar',
        array(
            'get_callback'    => 'get_calendar_functions',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_calendar_functions( $object, $field_name, $request ) {

    $calendar = new stdClass();
//    $calendar->recurrent = get_field('acf_calendar_recurrent',$object['id']);
    $calendar->recurrentDates = Array();

    $sessions = get_field('session'); // get all the rows
    $start_date = new DateTime($sessions[0]['date']);
//    $end_date = new DateTime($sessions[count($sessions)]['date']);

    if (have_rows('recurring_dates',$object['id'])): // check if the flexible content field has rows of data
        while ( have_rows('recurring_dates', $object['id']) ) : the_row(); // loop through the rows of data
            array_push($calendar->recurrentDates, new DateTime(get_sub_field('recurring_date')));
        endwhile;
    endif;

    $calendar->startDate = $start_date;
//    $calendar->endDate = $end_date;



    return $calendar;
}
