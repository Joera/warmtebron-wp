<?php

add_action( 'init', 'register_interviewee' );

function register_interviewee() {
    register_rest_field( array('post'),
        'interviewee',
        array(
            'get_callback'    => 'get_interviewee',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_interviewee( $object, $field_name, $request )
{
    $interviewee = new stdClass();
    $interviewee->name = get_field('interviewee_name', $object['id']);
    $interviewee->organisation = get_field('interviewee_organisation', $object['id']);


    return $interviewee;
}
