<?php

add_action( 'init', 'register_language' );

function register_language() {
    register_rest_field( array('post','page'),
        'language',
        array(
            'get_callback'    => 'get_language',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_language( $object, $field_name, $request )
{
//    $languageObject = apply_filters( 'wpml_post_language_details', NULL, $object['id'] ) ;
//    $lan = $languageObject['language_code'];
//
//    // if has translation include id & slug of translation
//    if($lan === 'en') {
//        $translationID = apply_filters( 'wpml_object_id', $object['id'] , $object['type'],false,'nl');
//    } else {
//        $translationID = apply_filters( 'wpml_object_id', $object['id'] , $object['type'],false,'en');
//    }
//
//    $translation = get_post($translationID);
//
//    if($translationID == '9159') {
//
//        $permalink = WP_HOME . '/english';
//
//    } else if ($translationID != null) {
//        $permalink = wph_replace_permalink(get_permalink($translationID), $translationID);
//    } else if ($languageObject['language_code'] == 'en'){
//        $permalink = WP_HOME;
//    } else {
//        $permalink = WP_HOME . '/english';
//    }

    return array(
        'code' => 'nl' // $languageObject['language_code'],
//        'translation_id' => $translationID,
//        'translation_slug' => $translation->post_name,
//        'translation_url' => $permalink
    );
}
