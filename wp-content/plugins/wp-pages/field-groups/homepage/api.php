<?php

add_action( 'init', 'register_homepage_content' );

function register_homepage_content() {
    register_rest_field( array('page'),
        'featured_theme',
        array(
            'get_callback'    => 'get_featured_theme',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_featured_theme( $object, $field_name, $request ) {

    if ($object['title']['rendered'] == 'Homepage' OR $object['title']['rendered'] == 'English') {
        return get_post_meta($object['id'],'featured_theme',true);
    } else {
        return false;
    }
}
