<?php

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_uitgelicht',
		'title' => 'Uitgelicht',
		'fields' => array (
			array (
				'key' => 'field_59a58e58ea019',
				'label' => 'Thema',
				'name' => 'featured_theme',
				'type' => 'post_object',
				'post_type' => array (
					0 => 'theme',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
                array (
                    'param' => 'page',
                    'operator' => '==',
                    'value' => '1401',
                    'order_no' => 0,
                    'group_no' => 0,
                )
			),
            array (
                array (
                    'param' => 'page',
                    'operator' => '==',
                    'value' => '9159',
                    'order_no' => 0,
                    'group_no' => 0,
                )
            )
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}