<?php

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'acf_sessions_repeater',
        'title' => 'Sessies',
        'fields' => array(
            array(
                'key' => 'acf_session',
                'label' => 'Sessie',
                'name' => 'session',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 1,
                'max' => 0,
                'layout' => 'row',
                'button_label' => 'Extra sessie',
                'sub_fields' => array(

                    array(
                        'key' => 'acf_session_tab_session',
                        'label' => 'Info',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                    ),
                    array(
                        'key' => 'acf_session_name',
                        'label' => 'Naam',
                        'name' => 'name',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
//                    array(
//                        'key' => 'acf_session_tab_times',
//                        'label' => 'Tijden',
//                        'name' => '',
//                        'type' => 'tab',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => array(
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ),
//                        'placement' => 'top',
//                        'endpoint' => 0,
//                    ),

                    array(
                        'key' => 'acf_session_date',
                        'label' => 'Datum',
                        'name' => 'date',
                        'type' => 'date_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => 'half',
                            'id' => '',
                        ),
                        'display_format' => 'd-m-Y  ',
                        'return_format' => 'Y-m-d',
                        'first_day' => 1,
                    ),
                    array(
                        'key' => 'acf_session_start_time',
                        'label' => 'Aanvangstijd',
                        'name' => 'start_time',
                        'type' => 'time_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => 'half',
                            'id' => '',
                        ),
                        'display_format' => 'H:i',
                        'return_format' => 'H:i',
                        'first_day' => 1,
                    ),
                    array(
                        'key' => 'acf_session_end_time',
                        'label' => 'Eindtijd',
                        'name' => 'end_time',
                        'type' => 'time_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'display_format' => 'H:i',
                        'return_format' => 'H:i',
                        'first_day' => 1,
                    ),
                    array(
                        'key' => 'acf_session_available_spots',
                        'label' => 'Beschikbare plaatsen',
                        'name' => 'session_available_spots',
                        'type' => 'number',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'acf_session_max_count',
                                    'operator' => '!=empty',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'esc_html' => 0,
                        'readonly' => 1
                    ),
                    array(
                        'key' => 'acf_session_tab_location',
                        'label' => 'Locatie',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                    ),

                    array(
                        'key' => 'acf_session_location_name',
                        'label' => 'Naam locatie',
                        'name' => 'location_name',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),

                    array(
                        'key' => 'acf_session_location_adress',
                        'label' => 'Adres locatie',
                        'name' => 'location_address',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),

                    array(
                        'key' => 'acf_session_location',
                        'label' => 'Locatie',
                        'name' => 'location',
                        'type' => 'google_map',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'center_lat' => '52.33818',
                        'center_lng' => '4.8712359',
                        'zoom' => '',
                        'height' => '',
                    ),
                    array(
                        'key' => 'acf_session_tab_guests',
                        'label' => 'Aanmeldingen',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                    ),
                    array(
                        'key' => 'acf_session_max_count',
                        'label' => 'Maximum aantal deelnemers',
                        'name' => 'session_max_count',
                        'type' => 'number',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'acf_session_guests',
                        'label' => 'Deelnemers',
                        'name' => 'session_guest',
                        'type' => 'repeater',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'collapsed' => '',
                        'min' => 0,
                        'max' => 0,
                        'layout' => 'row',
                        'button_label' => 'Nieuwe deelnemer',
                        'sub_fields' => array(
                            array(
                                'key' => 'acf_session_guest_name',
                                'label' => 'Naam',
                                'name' => 'session_guest_name',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'acf_session_guest_email',
                                'label' => 'Email',
                                'name' => 'session_guest_email',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'acf_session_guest_phone',
                                'label' => 'Telefoon',
                                'name' => 'session_guest_phone',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'acf_session_guest_count',
                                'label' => 'Aantal',
                                'name' => 'session_guest_count',
                                'type' => 'number',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),

                        ),
                    ),
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'activity',
                    'order_no' => 1,
                    'group_no' => 1,
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'no_box',
        'label_placement' => 'none',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;