<?php

add_action( 'init', 'register_sessions' );

function register_sessions() {
    register_rest_field( array('activity'),
        'sessions',
        array(
            'get_callback'    => 'get_sessions',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_sessions( $object, $field_name, $request ) {

    $sessions = new stdClass;
    $order = 0; // order of the section
    $previous = '';

    if (have_rows('session',$object['id'])): // check if the flexible content field has rows of data
        while ( have_rows('session', $object['id']) ) : the_row(); // loop through the rows of data
            $sessions->$order = new stdClass;
            $sessions->$order->name = get_sub_field('name');
            $sessions->$order->date = new DateTime(get_sub_field('date'));
            $sessions->$order->start_time = get_sub_field('start_time');
            $sessions->$order->end_time = get_sub_field('end_time');

            $sessions->$order->max_spots = get_sub_field('session_max_count');

            $available_spots = get_sub_field('session_available_spots');

            if (!isset($available_spots) || empty($available_spots)) {
                $sessions->$order->available_spots = $sessions->$order->max_spots;
            } else {
                $sessions->$order->available_spots = $available_spots;
            }

            $location = get_sub_field('location');
            $location_name = get_sub_field('location_name');
            $location_address = get_sub_field('location_address');

            if($location) {
                $location['name'] = $location_name;
                $location['submitted_address'] = $location_address;
            } else if($location_name) {
                $location = new stdClass();
                $location->name = $location_name;
                $location->submitted_address = $location_address;
            }

            $sessions->$order->location = $location;

            $sessions->$order->guests = array();

            if ( have_rows('session_guest', $object['id'])):
            while ( have_rows('session_guest', $object['id']) ) : the_row();

                $guest = new stdClass;
                $guest->name = get_sub_field('session_guest_name');
                $guest->email = get_sub_field('session_guest_email');
                $guest->phone = get_sub_field('session_guest_phone');
                $guest->count = get_sub_field('session_guest_count');

                array_push($sessions->$order->guests,$guest);

            endwhile;
            endif;

            $order++;

        endwhile;
        return $sessions;
    else :
        return null;
    endif;


}
