<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_thema-velden',
        'title' => 'Thema velden',
        'fields' => array (
            array (
                'key' => 'field_58e35efd67c08',
                'label' => 'Uitgelichte nieuwsberichten en/of bouwprojecten',
                'name' => 'featured-content',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_58e35f3267c09',
                        'label' => 'Bericht',
                        'name' => 'blog',
                        'type' => 'post_object',
                        'column_width' => '',
                        'post_type' => array (
                            0 => 'page',
                            1 => 'post',
                            2 => 'construction-project'
                        ),
                        'taxonomy' => array (
                            0 => 'all',
                        ),
                        'allow_null' => 1,
                        'multiple' => 0,
                    ),
                ),
                'row_min' => 2,
                'row_limit' => 2,
                'layout' => 'row',
                'button_label' => '',
            )
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                    'order_no' => 0,
                    'group_no' => 1,
                )
            )
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'metabox',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

?>