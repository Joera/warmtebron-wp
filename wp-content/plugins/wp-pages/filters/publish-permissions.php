<?php

function check_publish_permissions($post_id) {

    $type = get_post_type($post_id); // get post type
    $new_status = get_post_status($post_id); // get new status
    $old_status = get_post_meta($post_id, 'old_status', true); // get previous status

    if ($type == 'acf' || $type == 'feed' || $type == 'subscriber' || $type == 'partner' || $type == 'contact' || $type == 'revision') {
        return false;
    } else {
        return true;
    }
}

add_filter( 'publish_permissions', 'check_publish_permissions', 12, 3 );