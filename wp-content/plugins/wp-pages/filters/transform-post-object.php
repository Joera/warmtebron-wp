<?php

function change_template_names($data) {

    if($data['type'] == 'page' && $data['slug'] == 'projectteam') {
        $data['type'] = 'projectteam';
    }

    unset($data['_links']);
    return $data;
}

//function add_sort_date($data) {
//
//    $data['sort_date'] = $data['date'];
//
//    if($data['updates'] && !empty($data['updates'])) {
//        $data['sort_date'] = $data['updates'][0]->date;
//    }
//
//
//    return $data;
//}

add_filter( 'transform-post-object', 'change_template_names', 12, 3 );
//add_filter( 'transform-post-object', 'add_sort_date', 12, 3 );