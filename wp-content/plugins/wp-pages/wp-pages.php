<?php
/*
Plugin Name: WP PAGES
Plugin URI: zuidas.nl
Description: extending CORE functionality
Author: Joera
Author URI: zuidas.nl
Version: 0.1
*/

/* Disallow direct access to the plugin file */

	if (basename($_SERVER['PHP_SELF']) == basename (__FILE__)) {
		die('Sorry, but you cannot access this page directly.');
	}

	// content types
	include 'content-types/contactperson.php';
    include 'content-types/partner.php';

// custom field groups

	include 'field-groups/language/api.php';

    include 'field-groups/contactperson/acf.php';
    include 'field-groups/contactperson/api.php';

    include 'field-groups/interviewee/acf.php';
    include 'field-groups/interviewee/api.php';

    include 'field-groups/external-link/acf.php';
    include 'field-groups/external-link/api.php';

    include 'field-groups/featured-item/acf.php';
    include 'field-groups/featured-item/api.php';

    include 'field-groups/subtitle/acf.php';
    include 'field-groups/subtitle/api.php';

    include 'field-groups/interaction/match_comment_author.php';



	// wp admin tweaks
	include 'wp-admin-tweaks/cors.php';
//	include 'wp-admin-tweaks/columns_comment_ratings.php';
	include 'wp-admin-tweaks/wph-permalinks.php';
	include 'wp-admin-tweaks/admin_css.php';
    include 'wp-admin-tweaks/image_editor.php';
    include 'wp-admin-tweaks/allow_svg_upload.php';

    include 'filters/transform-post-object.php';
    include 'filters/publish-permissions.php';
    include 'filters/log-post-object.php';