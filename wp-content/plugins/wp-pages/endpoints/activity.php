<?php
defined( 'ABSPATH' ) or die( 'You can\'t access this file directly!');

class WPH_Calendar {
    public function init() {
        $this->hooks();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_submit_activity_endpoint'));
    }

    public function register_submit_activity_endpoint() {
        register_rest_route( 'wp/v2', '/submit_activity/', array(
            'methods' => 'POST',
            'callback' => array($this, 'insert_activity')
        ) );
    }

    public function insert_activity($request) {

        $params = $request->get_params();

        PC::debug($params);

        // Check if message is not empty
        if (!isset($params['title']) || empty($params['title']))
            return array('status' => 400, 'message' => __('Make sure to write a title.', 'wph-calendar'));

        if (!isset($params['content']) || empty($params['content']))
            return array('status' => 400, 'message' => __('Make sure to write a content.', 'wph-calendar'));

        if (!isset($params['date']) || empty($params['date']))
            return array('status' => 400, 'message' => __('Make sure to write a date.', 'wph-calendar'));

        if (!isset($params['start_time']) || empty($params['start_time']))
            return array('status' => 400, 'message' => __('Make sure to write a start time.', 'wph-calendar'));

        if (!isset($params['end_time']) || empty($params['end_time']))
            return array('status' => 400, 'message' => __('Make sure to write a end time.', 'wph-calendar'));

//        if (!isset($params['location']) || empty($params['location']))
//            return array('status' => 400, 'message' => __('Make sure to write a location.', 'wph-calendar'));

        $args = array(
            'post_title' => $params['title'],
            'post_type' => 'activity',
            'post_author' => 16,   // 16
            'post_status' => 'draft',
            'post_content' => $params['content']
        );

        $session_fields = array(

            'date' => $params['date'],
            'start_time' => $params['start_time'],
            'end_time' => $params['end_time'],
            'location_name' => $params['location_name'],
            'location_address' => $params['address_street'] . ' ' . $params['address_nr'],

//            'location' => array (
//                'address' => $params['address_street'] . ' ' . $params['address_nr'],
//                'lat' => null,
//                'lng' => null
//            )
        );

        $contact = array(

            'name' => $params['contact_name'],
            'organisation' => $params['contact_organisation'],
            'email' => $params['contact_email'],
            'phone' => $params['contact_phone'],
            'url' => $params['contact_url']
        );

        $postID = wp_insert_post($args);

        if (is_wp_error($postID)) {
            return array('status' => 400, 'message' => __('Something went wrong.', 'wph-calendar'));
        } else {
            update_field('acf_session', array($session_fields), $postID);

            foreach($contact as $key => $value) {

                update_field('acf_contact_person_' . $key, $value, $postID);
            }

          //  wp_notify_postauthor($insert_comment);
        }

        return array('status' => 200, 'message' => __('Your activity has been submitted.', 'wph-calendar'));
    }
}

$wph_calendar = new WPH_Calendar();
$wph_calendar->init();