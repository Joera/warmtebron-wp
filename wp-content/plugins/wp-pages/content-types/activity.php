<?php

function register_activity() {

    // register theme custom post type
    $args = array(
        'public' => true,
        'label'  => 'Agenda',
        'labels' => array(
            'add_new_item' => 'Activiteit toevoegen',
            'new_item' => 'Nieuwe activiteit',
            'view_item' => 'Bekijk activiteit',
            'view_items' => 'Bekijk activiteiten'
        ),
        "show_in_rest" => true,
        "rest_base" => "activity",
        "has_archive" => false,
        "show_in_menu" => true,
        "menu_position" => 5,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => true,
        "rewrite" => array( "slug" => "activity", "with_front" => true ),
        "query_var" => true,
        'supports' => array('author','title','editor','thumbnail','revisions','page-attributes'),
    );
    register_post_type( 'activity', $args );
}

add_action( 'init', 'register_activity' );