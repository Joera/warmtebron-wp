<?php

function register_partner() {

    // register theme custom post type
    $args = array(
        'public' => true,
        'label'  => 'Partners',
        'labels' => array(
            'add_new_item' => 'Partner toevoegen',
            'new_item' => 'Nieuwe partner',
            'view_item' => 'Bekijk partners',
            'view_items' => 'Bekijk partners'
        ),
        "show_in_rest" => true,
        "rest_base" => "partner",
        "has_archive" => false,
        "show_in_menu" => true,
        "menu_position" => 6,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => true,
        "rewrite" => array( "slug" => "partner", "with_front" => true ),
        "query_var" => true,
        'taxonomies' => array('category'),
        'supports' => array('title','editor','revisions','page-attributes'),
    );
    register_post_type( 'partner', $args );
}

add_action( 'init', 'register_partner' );