<?php

function register_contact() {

    // register theme custom post type
    $args = array(
        'public' => true,
        'label'  => 'Contact',
        'labels' => array(
            'add_new_item' => 'Persoom toevoegen',
            'new_item' => 'Nieuwe contactpersoon',
            'view_item' => 'Bekijk contactpersonen',
            'view_items' => 'Bekijk contactpersonen'
        ),
        "show_in_rest" => true,
        "rest_base" => "contact",
        "has_archive" => false,
        "show_in_menu" => true,
        "menu_position" => 5,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => true,
        "rewrite" => array( "slug" => "contact", "with_front" => true ),
        "query_var" => true,
        'supports' => array('title','editor','revisions','page-attributes'),
    );
    register_post_type( 'contact', $args );
}

add_action( 'init', 'register_contact' );