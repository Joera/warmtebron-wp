<?php

include('partials/header.php');
include('partials/footer.php');
include('partials/beeldmerk.php');

$html = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Zuidas houdt je op de hoogte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <style type="text/css">
            body {
                font-family: Arial;
                font-size: 14px;
            }

            v\:* { behavior: url(#default#VML); display:inline-block; }


            h2 > span > span {
                position: relative;
                right: 10px;
            }

            .follow-us {
                width: 108px;
                margin: 20px auto 5px auto;
                font-size: 14px;
                line-height: 20px;
                color: #000000;
            }

            .social-icon {
                text-decoration:none;
                margin: 0 3px;
            }
            .footerLinks {
                font-size: 13px;
                line-height: 20px;
                text-align:center;
            }
            .footerLinks p {
                width:500px;
                display:block;
                color: #000;
                margin: 0 auto 7px auto;
            }
            .footerlinks b {
                width:200px;
                text-align:center;
                display:block;
                color: #000;
                font-size: 14px;
                margin:0px auto 0 auto;
            }
            .footerLink {
                display:block;
                width: 240px;
                color:#000;
                margin: 0 auto 20px auto;
            }


    </style>
</head>

<body style="margin: 0; padding: 0; font-family: Arial; font-size: 14px;">
<table border="0" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                <tr>

                    <td >
                        <table align="center" border="0" cellpadding="0" cellspacing="0">

                            ' . $header . '
                            
                            ' . $beeldmerk . '

                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="560" style="margin: 0 auto;">
                                        <tr>
                                            <td style="padding: 30px 0 0 0"; width="560" align="left">
                                            
                                                <p style="font-size:13px;line-height:20px;margin:0px auto 20px auto;color:#000;">
                                                    Er is een nieuwe reactie geplaatst bij het bericht "' . $post->post_title . '". Wilt u bij een volgende reactie geen bericht meer ontvangen?
                                                    Klik dan <a href="'  . WP_HOME . '/api/respond/unsubscribeToComments?email=' . $email . '&postId=' . $post->ID .'">hier.</a></p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                                        
                            <!-- Content -->

                            <tr>
                                <td style="padding: 0px;">

                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="560" style="margin:0 auto;">
                                        <tr>
                                            <td style="padding: 20px 0px 40px 0px;">
                                                <span style="font-size:13px;line-height:20px;width:440px;color: #000000;">' . $comment->comment_author . ' schrijft:</span>
                                                <div style="font-size:13px;line-height:20px;width:440px;color: #000000;">' . $comment->comment_content  . '</div>
                                             
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="560" style="margin:0 auto 30px auto;">
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                        <tr align="center">
                                            
                                            <td width="160" align="center">
                                                <span style="background: #dedc00; color: #000; border-radius: 20px; display: block; width: 220px; height: 36px; line-height: 36px; text-align: center; color: #000000; margin: 0px auto 30px auto; text-decoration: none;">
                                                    <a href="' . $post_url . '" bgcolor="#ffffff" style="color: #000000; text-decoration: none;">lees verder op warmtebron.nu</a>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

         
                            <!-- Footer -->
                             ' . $footer . '
                        </table>

                    </td>
                    <td width="20">
                        </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>
';

?>
