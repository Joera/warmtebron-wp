<?php

$text = '<span style="color:#e6007e;border-bottom:1px solid #e6007e;">Bedankt voor je inschrijving</span><br><br>We houden je op de hoogte van de ontwikkelingen op warmtebron.nu <br><br>
        Via onderstaande link kun je gemakkelijk instellen hoe vaak je van ons wilt horen.<br><br>';
$button = 'Stel je voorkeuren in';

include('partials/header.php');
include('partials/footer.php');
include('partials/beeldmerk.php');

$html = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>' . $subject . '</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0; font-family: Arial; font-size: 14px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                            
                            ' . $beeldmerk . '

                            <!-- Content -->
                            <tr>
                                <td style="padding: 40px 0px 20px 0px; font-size:13px;line-height:20px;color:#000000">
                                   ' . $text . '
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                                        <tr>
                                            <td>
                                                 <a href="' . WP_HOME . '?newsletter-token=' . $token . '"bgcolor="#dedc00" style="background: #dedc00; color: #000; border-radius: 20px; display: block; width: 160px; height: 36px; line-height: 36px; text-align: center; color: #000000; margin: 0px auto 30px auto; text-decoration: none;">' . $button . '</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                          

                        </table>
                    </td>
                </tr>
               
  
                ' . $footer . '
                      
                
            </table>
        </td>
    </tr>
</table>
</body>

</html>
';