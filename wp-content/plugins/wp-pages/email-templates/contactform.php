<?php


include('partials/beeldmerk.php');

$html = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Zuidas houdt je op de hoogte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <style type="text/css">
            body {
                font-family: Arial;
                font-size: 14px;
            }

            v\:* { behavior: url(#default#VML); display:inline-block; }


            h2 > span > span {
                position: relative;
                right: 10px;
            }

            .follow-us {
                width: 108px;
                margin: 20px auto 5px auto;
                font-size: 14px;
                line-height: 20px;
                color: #000000;
            }

            .social-icon {
                text-decoration:none;
                margin: 0 3px;
            }
            .footerLinks {
                font-size: 13px;
                line-height: 20px;
                text-align:center;
            }
            .footerLinks p {
                width:500px;
                display:block;
                color: #000;
                margin: 0 auto 7px auto;
            }
            .footerlinks b {
                width:200px;
                text-align:center;
                display:block;
                color: #000;
                font-size: 14px;
                margin:0px auto 0 auto;
            }
            .footerLink {
                display:block;
                width: 240px;
                color:#000;
                margin: 0 auto 20px auto;
            }


    </style>
</head>

<body style="margin: 0; padding: 0; font-family: Arial; font-size: 14px;">
<table border="0" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                <tr>

                    <td >
                        <table align="center" border="0" cellpadding="0" cellspacing="0">

                
                            
                            ' . $beeldmerk . '

                            
                            <!-- Content -->

                             <tr>
                                <td style="padding: 40px 0px 0px 0px;">
                                    <p>Via het contactformulier op de website is een vraag of klacht verstuurd door:</p>
                                    <span>Naam: </span>' . $name .' <br/>
                                    <span>Email: </span>' . $email .' <br/>
                                </td>
                            </tr>
                            <tr>

                                <td style="padding: 20px 0px 40px 0px;">


                                    ' . $text .'

                                </td>
                            </tr>

                            ' . $beeldmerk . '
                            <!-- Footer -->
              
                        </table>

                    </td>
                    <td width="20">
                        </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>
';