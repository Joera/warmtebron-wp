<?php


include('partials/header.php');
include('partials/footer.php');
include('partials/beeldmerk.php');
include('partials/introduction.php');

$html = '
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
       <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <title>Zuidasbouwt</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          
          <style type="text/css">
                    body {
                        font-family: Arial;
                        font-size: 14px;
                    }
        
                    v\:* { behavior: url(#default#VML); display:inline-block; }
        
        
                    h2 > span > span {
                        position: relative;
                        right: 10px;
                    }
        
                    .follow-us {
                        width: 108px;
                        margin: 20px auto 5px auto;
                        font-size: 14px;
                        line-height: 20px;
                        color: #000000;
                    }
        
                    .social-icon {
                        text-decoration:none;
                        margin: 0 3px;
                    }
                    .footerLinks {
                        font-size: 13px;
                        line-height: 20px;
                        text-align:center;
                    }
                    .footerLinks p {
                        width:500px;
                        display:block;
                        color: #000;
                        margin: 0 auto 7px auto;
                    }
                    .footerlinks b {
                        width:200px;
                        text-align:center;
                        display:block;
                        color: #000;
                        font-size: 14px;
                        margin:0px auto 0 auto;
                    }
                    .footerLink {
                        display:block;
                        width: 240px;
                        color:#000;
                        margin: 0 auto 20px auto;
                    }
        
            </style>
      </head>

      <body style="margin: 0; padding: 0; font-family: Arial; font-size: 14px;">
          <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                  <td>
                      <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                          <tr>
                              <td>
                                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">

                                      <!-- Top spacing -->
                                     ' . $header . '
                            
                                     ' . $beeldmerk . '
                            
                                     ' . $introduction . '

                                      <!-- Content -->
                                      <tr>
                                          <td style="padding: 20px auto 40px auto;">

                                              <table valign="top" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                                              ' . $post_html . '
                                              </table>

                                          </td>
                                      </tr>
                                      
                                       <tr>
                                            <td>
                                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="560" style="margin:0 auto 30px auto;">
                                                    <tr align="center">
                                                        <td width="160" align="center" style="margin:0 auto;" >
                                                            <span style="text-align:center;border: 1px solid #000000;padding: 5px 20px;">
                                                                <a href="' . WP_HOME . '" bgcolor="#ffffff" style="color: #000000; text-decoration: none;">Meer op Zuidas.nl</a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        
                                        ' . $beeldmerk . '
                                        <!-- Footer -->
                                         ' . $footer . '

                                 
                                  </table>

                              </td>
                          </tr>
                      </table>
                  </td>
              </tr>
          </table>
      </body>

      </html>
    ';

?>