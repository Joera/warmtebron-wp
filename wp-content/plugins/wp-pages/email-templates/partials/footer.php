<?php

$footer = '

    <!-- Footer -->
    <tr>
        <td>
            <table class="footer" align="center" border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#e8ece9">
             <!--  <tr>
                    <td width="108" style="margin: 0 auto;" align="center">
                        <div class="MsoNormal" style="width: 108px; margin: 20px auto 5px auto;text-align:center"><b style="font-size: 14px;line-height: 20px; color: #000000 ">Volg ons ook op</b></div>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="margin: 0 auto;" >
                        <table align="center" class="MsoNormal" width="110" style="margin: 0 auto 0px auto; text-align:center;">
                            <tr>                            <!--    <td width="28">
                                   <a class="social-icon" href="https://www.facebook.com/zuidas/" target="_blank" class="MsoNormal" style="text-decoration:none; display:inline;">
                                            <img src="https://ucarecdn.com/78f8c779-e788-4989-a294-9d852eff6886/-/resize/56/email_fb_II_2x.jpg" width="28" height="28">                                    </a>                                </td>                               <td width="2">
                               </td> 
                                <td width="36">
                                    <a class="social-icon" href="https://twitter.com/WarmtebronU" target="_blank" class="MsoNormal" style="text-decoration:none; display:inline; border-radius: 100%;">
                                           <img src="https://ucarecdn.com/3593f614-2c15-457c-8076-689a59913918/-/resize/56/email_tw_2x.jpeg" width="28" height="28">
                                    </a>
                                </td>
                                <td width="8">
                                </td>
                                <td width="36">
                                    <a class="social-icon" href="https://www.linkedin.com/company/warmtebronutrecht" target="_blank" class="MsoNormal" style="text-decoration:none; display:inline;border-radius: 100%">
                                        <img src="https://ucarecdn.com/d5965793-9c60-460b-97ee-ae6859597152/-/resize/56/email_linkedin_2x.jpeg" width="28" height="28">
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr> -->
                <tr>
                    <td height="20">
                    </td>
                </tr> 
                <tr class="links">
                    <td class="footerLinks MsoNormal" style="font-size: 13px; line-height: 20px; text-align:center;">
                        <div class="MsoNormal" style="width:500px;display:block;color: #000;margin: 0 auto 7px auto; font-size: 14px; ">Je ontvangt deze mail omdat je aangaf op de hoogte te willen blijven over Warmtebron Utrecht.</div>
                    <!--    <div class="footerLink" style="margin: 0 auto; width: 240px; height: 20px;">
                            <a href="' . WP_HOME . '?newsletter-token=' . $token . '" class="MsoNormal" style="color:#000;">> Instellingen aanpassen</a>
                        </div> -->
                        <div class="footerLink" style="margin: 0 auto 20px auto; width: 240px; height: 20px;">
                            <a href="' . WP_HOME . '?newsletter-token=' . $token . '&action=unsubscribe" class="MsoNormal" style="color:#000;">> Uitschrijven</a>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
    <!-- Unsubscribe -->
    
        
';

?>