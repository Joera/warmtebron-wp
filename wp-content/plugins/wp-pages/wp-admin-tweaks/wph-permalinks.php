<?php
/* 
* Can I please click those links?
*
*/
add_filter('post_link', 'wph_replace_permalink', 10, 3 );
add_filter('page_link', 'wph_replace_permalink', 10, 3 );
add_filter('post_type_link', 'wph_replace_permalink', 10, 3 );

function wph_replace_permalink($url, $post, $leavename = false) {
    $parsed = parse_url(get_site_url());

    $urlbase = $parsed['scheme'].'://'.$parsed['host'].'/';


    if (is_int($post)) {
        $post = get_post($post);
    }
    $languageObject = apply_filters( 'wpml_post_language_details', NULL, $post->ID ) ;

    if($languageObject['language_code'] == 'en') {
        $urlbase = $urlbase . 'en/';
    }

    if ($post->ID == 1401) {
        $url = $urlbase;
    }

    else if ($post->ID == 9159) {
        $url = str_replace("en/","", $urlbase) . 'english';

    }

    else if ( $post->post_type == 'post' ) {
        $year = date("Y", strtotime($post->post_date));
		$url = $urlbase.'nieuws/'.$year.'/'.$post->post_name.'/';
    }

    else if ($post->post_type == 'page') {
        $url = $urlbase.$post->post_name.'/';
    }


	return $url;
};