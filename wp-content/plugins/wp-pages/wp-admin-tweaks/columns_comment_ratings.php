<?php
// https://www.smashingmagazine.com/2012/05/adding-custom-fields-in-wordpress-comment-form/
// http://justintadlock.com/archives/2011/06/27/custom-columns-for-custom-post-types

  add_filter('comment_form_default_fields', 'custom_fields');
  function custom_fields($fields) {

      $commenter = wp_get_current_commenter();
      $req = get_option( 'require_name_email' );
      $aria_req = ( $req ? " aria-required='true'" : '' );

      $fields[ 'author' ] = '<p class="comment-form-author">'.
        '<label for="author">' . __( 'Name' ) . '</label>'.
        ( $req ? '<span class="required">*</span>' : '' ).
        '<input id="author" name="author" type="text" value="'. esc_attr( $commenter['comment_author'] ) .
        '" size="30" tabindex="1"' . $aria_req . ' /></p>';

      $fields[ 'email' ] = '<p class="comment-form-email">'.
        '<label for="email">' . __( 'Email' ) . '</label>'.
        ( $req ? '<span class="required">*</span>' : '' ).
        '<input id="email" name="email" type="text" value="'. esc_attr( $commenter['comment_author_email'] ) .
        '" size="30"  tabindex="2"' . $aria_req . ' /></p>';

    return $fields;
  }

  // Save the comment meta data along with comment

  add_action( 'comment_post', 'save_comment_meta_data' );
  function save_comment_meta_data( $comment_id ) {

    if ( ( isset( $_POST['rating'] ) ) && ( $_POST['rating'] != '') ) {
      $rating = wp_filter_nohtml_kses($_POST['rating']);
      add_comment_meta( $comment_id, 'rating', 0 );
    }

  }


  // Add an edit option to comment editing screen  

  add_action( 'add_meta_boxes_comment', 'extend_comment_add_meta_box' );
  function extend_comment_add_meta_box() {
      add_meta_box( 'assignedto', __( 'Redactie' ), 'extend_comment_meta_box', 'comment', 'normal', 'high' );
  }


  function extend_comment_meta_box ( $comment ) {
      $rating = get_comment_meta( $comment->comment_ID, 'rating', true );
      wp_nonce_field( 'extend_comment_update', 'extend_comment_update', false );
      ?>

      <p>
          <?php echo $rating . ' stemmen'; ?>

      </p>

      <?php
  }

  // Update comment meta data from comment editing screen 

  add_action( 'edit_comment', 'extend_comment_edit_metafields' );

  function extend_comment_edit_metafields( $comment_id ) {
      if( ! isset( $_POST['extend_comment_update'] ) || ! wp_verify_nonce( $_POST['extend_comment_update'], 'extend_comment_update' ) ) return;
  }

  // display extra info in column on comment list 

  add_action('load-edit-comments.php', 'extra_comment_columns_load');
  
  function extra_comment_columns_load() {
      $screen = get_current_screen();
      add_filter("manage_{$screen->id}_columns", 'altered_comment_column_headers');
      add_action("manage_comments_custom_column", 'altered_comment_column_content',10,2);
  }
  
  function altered_comment_column_headers( $columns) {

    $columns = array(
      'cb' => '<input type="checkbox" />',
      'author' => __( 'Auteur' ),
      'comment' => __( 'Comment' ),
       'rating' => __( 'Waardering' )
    );

    return $columns;
  }


  function altered_comment_column_content($column,$commentID) {
    global $post;

    switch( $column ) {

        case 'rating' :

        $rating = get_comment_meta( $commentID, 'rating', true );

        if ($rating) :
            echo $rating;
        endif;
        break;

      /* Just break out of the switch statement for everything else. */
      default :
        break;
    }
  }

  // style columns

  add_action('admin_head', 'style_commenty_columns');

  function style_commenty_columns() {
      echo '<style type="text/css">';
      echo '.column-ratings { width: 120px !important; overflow: hidden;  }';
      echo '</style>';
  }



?>