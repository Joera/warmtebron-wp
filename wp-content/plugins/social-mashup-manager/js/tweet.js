// https://github.com/jublonet/codebird-js

// global var containing the retrieved tweet
var tweet = null;


/**
Get tweet from twitter rest api
*/
function getTweet() {
  // set search message
  document.getElementById("tweet-detail").innerHTML = 'Searching for tweet!';

  // get id of tweet that needs to be retreived
  var id = document.getElementById("tweet_id").value;

    var url = 'https://warmtebron.nu/sg-api/socials/fetch?id=' + id + '&source=twitter';

    axios.get(url)
        .then(function (response) {
            tweet = response.data; // .data.items[0];
            console.log(response);
            displayTweet();
        })
        .catch(function (error) {
            console.log(error);
           // document.getElementById("yt-detail").innerHTML = '<strong style="color: red;">Failed to fetch video!</strong>';
        });

}



/**
Display tweet
*/
function displayTweet() {
  var t = '';
  t += '<strong>By</strong><br>';
  t += tweet.user.name;
  t += '<br><br><strong>Datum</strong><br>';
  t += tweet.created_at;
  t += '<br><br><strong>Tweet</strong><br>';
  t += tweet.full_text;
  t += '<br><br><input type="submit" id="save_tweet" class="button" value="Save Tweet" onclick="saveTweet();">';
  document.getElementById("tweet-detail").innerHTML = t;
}



/**
Make update call to api to add tweet
*/
function saveTweet() {

    var image = '';

    if(tweet && tweet.entities && tweet.entities.media && tweet.entities.media[0]) {
        image = tweet.entities.media[0].media_url_https;
    }

  var object = {
      _id : tweet.id_str,
      objectID : tweet.id_str,
      source : "twitter",
      authorId : tweet.user.id,
      authorName: tweet.user.name,
      authorProfileImage: tweet.user.profile_image_url_https,
      creationDate: tweet.created_at,
      publishDate: null,
      date : tweet.created_at,
      image: image,
      text: tweet.full_text,
      sourceData : tweet
  };


  var url = API_URL + "/socials",
      config = {
        headers: {'Authorization': API_KEY}
      };

  axios.post(url, object, config)
    .then(function (response) {
      // console.log(response);
      window.location.href = PAGE_URL.replace('&action=addtweet', ''); // redirect to social manager overview page
    })
    .catch(function (error) {
      console.log(error);
      document.getElementById("tweet-detail").innerHTML = '<strong style="color: red;">Failed to save tweet!</strong>';
    });
}
