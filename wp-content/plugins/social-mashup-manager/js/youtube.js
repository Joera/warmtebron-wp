
var youtube = null;

function getYouTube() {
  // set search message
  document.getElementById("yt-detail").innerHTML = 'Searching for the video!';

  // get id of tweet that needs to be retreived
  var id = document.getElementById("yt_id").value;

  var key = 'AIzaSyDKv1R8Lxp5EiQQoEc_v-odg6IcV69TZng';

   var url = 'https://www.googleapis.com/youtube/v3/videos?id=' + id + '&part=snippet,contentDetails,statistics&key=' + key;


    axios.get(url)
        .then(function (response) {
            youtube = response.data.items[0];
            displayYouTube();
         //   window.location.href = PAGE_URL.replace('&action=addyoutube', ''); // redirect to social manager overview page
        })
        .catch(function (error) {
            console.log(error);
            document.getElementById("yt-detail").innerHTML = '<strong style="color: red;">Failed to fetch video!</strong>';
        });
}



/**
Display tweet
*/
function displayYouTube() {
  var t = '';
  t += '<strong>By</strong><br>';
  t += youtube.snippet.channelTitle;
  t += '<br><br><strong>Datum</strong><br>';
  t += youtube.snippet.publishedAt;
  t += '<br><br><strong>Video</strong><br>';
  t += youtube.snippet.title;
  t += '<br><br><input type="submit" id="save_video" class="button" value="Save Video" onclick="saveYouTube();">';
  document.getElementById("yt-detail").innerHTML = t;
}



/**
Make update call to api to add tweet
*/
function saveYouTube() {

  var object = {
      _id : youtube.id,
      objectID : youtube.id,
      source : "youtube",
      authorId : youtube.snippet.channelId,
      authorName: youtube.snippet.channelTitle,
      authorProfileImage: '',
      creationDate: youtube.snippet.publishedAt,
      publishDate: null,
      date : youtube.snippet.publishedAt,
      image: youtube.snippet.thumbnails.high.url,
      text: youtube.snippet.title,
      sourceData : youtube
  };


  var url = API_URL + "/socials",
      config = {
        headers: {'Authorization': API_KEY}
      };

  axios.post(url, object, config)
    .then(function (response) {
      // console.log(response);
      window.location.href = PAGE_URL.replace('&action=addyoutube', ''); // redirect to social manager overview page
    })
    .catch(function (error) {
      console.log(error);
      document.getElementById("yt-detail").innerHTML = '<strong style="color: red;">Failed to save youtube!</strong>';
    });
}
