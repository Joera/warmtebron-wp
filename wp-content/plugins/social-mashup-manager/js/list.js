

  /***********************************************************************************
  ** Render functions
  ***********************************************************************************/
  /**
  Render the social items list
  */
  function renderList(data) {
    // set table header
    var tableHead = '<thead><tr><th class="column-cb check-column"></th><th class="column-date">Publish Date</th><th class="column-title column-primary">Tekst</th><th class="column-author">Bron</th><th class="column-author">Auteur</th><th class="column-date">Datum</th><th class="column-image">Afbeelding</th></tr></thead>';

    // generate table body
    var tableBody = '<tbody>';
    data.forEach(function(item) {

      var publishDate = (item.publishDate && item.publishDate !== null) ? '<input id="' + item.id + '" type="text" class="publishDate" value="' + moment(item.publishDate).format('YYYY/MM/DD HH:mm') + '">' : 'Not published',
      // var publishDate = (item.publishDate !== null) ? '<a href="" onclick="showPublishdateEditor(event, \'' + item.id + '\')">' + moment(item.publishDate).format('YYYY/MM/DD HH:mm') + '</a>' : 'Not published',
          date = moment(item.created_at).format('YYYY-MM-DD HH:mm'),
          checkboxChecked = (item.publishDate && item.publishDate !== null) ? ' checked' : '',
          img;

      if (item.image) {
          img = '<img src="' + item.image + '" style="width:100%;"/>';
      } else {
          img = 'geen';
      }

      // generate table row
      var tr = '<tr>';
      tr += '<td class="check-column"><input type="checkbox"' + checkboxChecked + ' onclick="publish(\'' + item.id + '\');" style="margin-left: 7px;"></td>';
      tr += '<td class="column-date" style="width: 21%;">' + publishDate + '</td>';
      tr += '<td class="column-title column-primary"><a href="https://twitter.com/' + item.authorId + '/status/' + item.id + '" target="_blank">' + item.text + '</a></td>';
      tr += '<td class="column-author">' + item.source + '</td>';
      tr += '<td class="column-author"><div style="word-wrap:normal">' + item.authorName + '</div><div><img src="' + item.authorProfileImage + '" alt="" title="" style="width:48px;height:48px;display:block;"></img></div></td>';
      tr += '<td class="column-date">' + date + '</td>';
      tr += '<td class="column-image">' + img + '</td>';
      tr += '</tr>';
      tableBody += tr; // add table row to table body
    })
    tableBody += '</tbody>';

    // add table content to DOM
    document.getElementById("sociallist").innerHTML = tableHead + tableBody;

    //
    renderDatepickerEvents();
  }



  /**
  Render paging controls
  */
  function renderPaging(data) {
    var numberOfPages = Math.ceil(data.count / limit);
    document.getElementById("paging-index").innerHTML = page + " of " + numberOfPages;
  }


  /**
  Render the datepickers
  */
  function renderDatepickerEvents() {
    $('.publishDate').datetimepicker({
      format:'YYYY-MM-DD HH:mm',
      formatTime:'HH:mm',
      formatDate:'YYYY-MM-DD',
      step: 5,
      onChangeDateTime:function(dp,$input){
        var id = $input.attr('id'),
            newPublishDate = $input.val();

        dataset.data.forEach(function(item) { // loop dataset
          if(String(item._id) === String(id)) { // find item
            if(moment(item.publishDate).format('YYYY-MM-DD HH:mm') !== newPublishDate) { // check if date is updated
              item.publishDate = newPublishDate; // update publish date local
              publish(id, newPublishDate); // make call to update publish date
            }

          }
        })

      }
    });
  }


  /**
  Render all
  */
  function render(data) {
    renderList(data );
    renderPaging(data);
  }



  /***********************************************************************************
  ** Data functions
  ***********************************************************************************/
  /**
  Make update call to api
  */
  function updateData(item) {
    var url = API_URL + "/socials",
        body = item,
        config = {
          headers: {'Authorization': API_KEY}
        };

    axios.put(url, body, config)
      .then(function (response) {
// console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }



  /**
  Get data from api
  */
  function getData(limit, page, source, query) {

    var url = API_URL + "/socials?source=" + source + "&limit=" + limit + "&page=" + page + "&q=" + query,
        config = {
          headers: {'Authorization': API_KEY}
        };


    axios.get(url, config)
      .then(function (response) {
        dataset = response.data;
        render(dataset);
      })
      .catch(function (error) {
        console.log(error);
      });
  }


  /***********************************************************************************
  ** Event handlers
  ***********************************************************************************/
  /**
  Update publish status of social item
  */
  function publish(id, publishDate) {

    // update dataset
    dataset.forEach(function(item) { // loop dataset
      if(String(item._id) === String(id)) { // find item
        // set publish date

        if(item.publishDate === null && !publishDate) {
          item.publishDate = moment().format();
        } else if(publishDate) {
          item.publishDate = publishDate;
        } else {
          item.publishDate = null;
        }

        updateData(item);
        renderList(dataset);
        // render(dataset);
      }
    })
  }



  /**
  Search handler
  */
  function search() {
    query = document.getElementById("search-social-input").value;
    page = 1; // reset page to make sure there is a result
    getData(limit, page, source, query);
  }



  /**
  Filter handler
  */
  function filter() {
    var el = document.getElementById("source-filter");
    source = el.options[el.selectedIndex].value;
    page = 1; // reset page to make sure there is a result
    getData(limit, page, source, query);
  }



  /**
  Change the numbe rof rows event handler
  */
  function changeRows() {
    var el = document.getElementById("paging-filter");
    limit = el.options[el.selectedIndex].value;
    page = 1; // reset page to make sure there is a result
    getData(limit, page, source, query);
  }



  /**
  Previous page event handler
  */
  function prevPage() {
    var numberOfPages = Math.ceil(dataset.count / limit);
    if(page > 1) {
      page--;
    } else {
      page = numberOfPages;
    }
    getData(limit, page, source, query);
  }



  /**
  Next page event handler
  */
  function nextPage() {
    var numberOfPages = Math.ceil(dataset.count / limit);
    if(page < numberOfPages) {
      page++;
    } else {
      page = 1;
    }
    getData(limit, page, source, query);
  }



  /***********************************************************************************
  ** Datetime picker settings
  ***********************************************************************************/

  $( function() {

      $.datetimepicker.setDateFormatter({
          parseDate: function (date, format) {
              var d = moment(date, format);
              return d.isValid() ? d.toDate() : false;
          },
          formatDate: function (date, format) {
              return moment(date).format(format);
          }
      });

  });



  /***********************************************************************************
  **
  ***********************************************************************************/
  var dataset = null,
      limit = 10,
      page = 1,
      source = "",
      query = "";


  getData(limit, page, source, query);
