<?php
/*
Plugin Name: Social Mashup Manager
Plugin URI: zuidas.nl
Description: Select the social content tht will populate the social mashup
Author: Joera en Tom
Author URI: zuidas.nl
Version: 0.1
*/

add_action( 'admin_menu', 'add_admin_menu_item' );

// add management menu item
function add_admin_menu_item() {
	add_menu_page( 'Socials', 'Socials', 'manage_options', 'social-mashup-manager/social-mashup-manager.php', 'load_admin_page', 'dashicons-share', 6 );
}

//
function load_admin_page(){
	$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; // url of the page

	// add global javascript constants
	echo '<script>';
	echo 'var API_URL = "https://warmtebron.nu/sg-api";';
	echo 'var API_KEY = "' . API_URL_FRONTEND_AUTH_KEY . '";';
	echo 'var PAGE_URL = "' . $url . '";';
	echo '</script>';

	$action = '';
	if (isset($_GET['action'])) {
		$action = $_GET['action'];
	}


	if($action == 'addtweet') {
		include 'html/tweet.html';
	} else if($action == 'addyoutube') {
        include 'html/youtube.html';
    } else {
		include 'html/list.html';
	}
}



?>
