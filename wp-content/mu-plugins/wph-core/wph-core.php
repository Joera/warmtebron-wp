<?php
/*
Plugin Name: Headless Core Wordpress
Plugin URI: wijnemenjemee.nl
Description: functies uit functions.php in theme folder
Author: Hortus
Author URI: wijnemenjemee.nl
Version: 0.1
*/

/* Disallow direct access to the plugin file */

if (basename($_SERVER['PHP_SELF']) == basename (__FILE__)) {
	die('Sorry, but you cannot access this page directly.');
}

$wph_config = file_get_contents(ABSPATH . "wp-content/plugins/wp-pages/wph_config.json");

$wph_json = json_decode($wph_config,true);
define('WPH_CONFIG', $wph_config);
define('PLUGIN_FOLDER', dirname( __FILE__ ) . '/../../plugins/');

//var_dump(json_decode(WPH_CONFIG)->available_core_field_groups);

// tweaking basic admin functionality
include 'wp-admin-tweaks/permit_geojson_upload.php';
include 'wp-admin-tweaks/remove_post_formats.php';
include 'wp-admin-tweaks/image-sizes.php';
include 'wp-admin-tweaks/subscriber_fields.php';
include 'wp-admin-tweaks/disable_gutenberg.php';
include 'wp-admin-tweaks/disable_login_hints.php';
include 'wp-admin-tweaks/report_notice.php';


include 'connectors/mailgun.php';
// Controllers
include 'controllers/publish_controller.php';
include 'controllers/email_controller.php';


include 'services/report-services.php';
include 'services/subscriber_services.php';
include 'services/email_services.php';
include 'services/email_store.php';

include 'content-types/subscriber.php';

// Lib files
include 'hooks/publish_hooks.php';
include 'hooks/preview_hooks.php';
include 'hooks/image_hooks.php';
include 'hooks/email_hooks.php';
include 'hooks/comment_hooks.php';

include 'endpoints/all-types.php';
include 'endpoints/submit-comment.php';
include 'endpoints/post-rating.php';
include 'endpoints/comment-rating.php';
include 'endpoints/subscriber.php';
//include 'endpoints/subscriptions.php';
include 'endpoints/contactform.php';
include 'endpoints/emails.php';

include 'field-groups/shared/functions.php';
include 'field-groups/shared/remove_properties.php';

include 'field-groups/core-elements/image.php';
include 'field-groups/core-elements/document.php';

foreach (json_decode(WPH_CONFIG)->available_core_field_groups as &$group) {
    foreach(glob(ABSPATH . 'wp-content/mu-plugins/wph-core/field-groups/' . $group . '/*.php') as $file){
        include $file;
    }
}

include 'taxonomies/api.php';

?>
