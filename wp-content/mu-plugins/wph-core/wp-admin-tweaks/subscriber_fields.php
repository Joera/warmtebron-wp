<?php

function hidePreviewButtonSaAdmin() {
    global $post;
    if($post != null && $post->post_type == 'subscriber') {
        echo '<style>
          #preview-action, #misc-publishing-actions, #misc-publishing-actions, #minor-publishing-actions, #titlediv > div.inside {
            display:none !important;
          }
        </style>';
    }
}
add_action('admin_head', 'hidePreviewButtonSaAdmin');

// change the placeholder for the title input field
function changeTitleText( $title ){
    $screen = get_current_screen();
    if  ( 'subscriber' == $screen->post_type ) {
        $title = 'Enter name of the subscriber';
    }
    return $title;
}
add_filter( 'enter_title_here', 'changeTitleText' );

// Add columns to subscriber overview
// add header for columns
function columns_head_subscriber($defaults) {
    $defaults['email'] = 'Email';
    $defaults['subscription'] = 'Voorkeuren';
    $defaults['language'] = 'Taal';
    return $defaults;
}
// add column content
function columns_content_subscriber($column_name, $post_ID) {
    if ($column_name == 'email') {
        $email = get_field( "email", $post_ID );
        echo $email;
    }
    if ($column_name == 'subscription') {
        $subscription = get_field( "subscription", $post_ID );
        switch ($subscription) {
            case "none":
                $subscription_value = "Niet ingeschreven";
                break;
            case "direct":
                $subscription_value = "Bij ieder nieuw artikel";
                break;
            case "weekly":
                $subscription_value = "Wekelijks";
                break;
            case "monthly":
                $subscription_value = "Maandelijks";
                break;
            default:
                $subscription_value = "";
        }
        echo $subscription_value;
    }
    if ($column_name == 'language') {
        $language = get_field( "language", $post_ID );
        switch ($language) {
            case "nl":
                $language_value = "nl";
                break;
            case "en":
                $language_value = "en";
                break;
            default:
                $language_value = "";
        }
        echo $language_value;
    }
}
add_filter('manage_subscriber_posts_columns', 'columns_head_subscriber', 10);
add_action('manage_subscriber_posts_custom_column', 'columns_content_subscriber', 10, 2);

// Remove default columns from subscriber overview
function columns_remove_category($defaults) {
    unset($defaults['date']);
    return $defaults;
}
add_filter('manage_subscriber_posts_columns', 'columns_remove_category');