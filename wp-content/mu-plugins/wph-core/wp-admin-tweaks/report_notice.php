<?php


    function is_edit_page($new_edit = null){
        global $pagenow;
        //make sure we are on the backend
        if (!is_admin()) return false;

        if($new_edit == "edit")
            return in_array( $pagenow, array( 'post.php',  ) );
        elseif($new_edit == "new") //check for new post page
            return in_array( $pagenow, array( 'post-new.php' ) );
        else //check for either new or edit
            return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
    }

    function sample_admin_notice__success() {

        global $pagenow;
        $urlbase = WP_HOME;

        if (is_admin() && $pagenow == 'post.php') {

            $report = get_transient('_cms_report');

            $post_id = $_GET['post'];

            if (is_edit_page('edit') && $report && $report->primary_id == $post_id) {

                ?>
                <div class="notice notice-success is-dismissible">
                    <ul>
                        <?php

                        if (count($report->warn) > 0) {

                            echo '<li><ul><li><b>waarschuwing</b></li>';
                            foreach ($report->warn as &$msg) {

                                echo '<li>';
                                echo $msg;
                                echo '</li>';
                            }
                            echo '</li></ul>';
                        }

                        if (count($report->error) > 0) {

                            echo '<li><ul><li><b>error</b></li>';
                            foreach ($report->error as &$msg) {

                                echo '<li>';
                                echo $msg;
                                echo '</li>';
                            }
                            echo '</li></ul>';
                        }

                        if (count($report->rendered) > 0) {

                            echo '<li><ul><li><b>html gecreeerd</b></li>';
                            foreach ($report->rendered as &$msg) {

                                echo '<li><a href="' . $urlbase . '/' . $msg . '" target="_blank">';
                                echo '/' . $msg;
                                echo '</a></li>';
                            }
                            echo '</li></ul>';
                        }


                        if (count($report->deleted) > 0) {

                            echo '<li><ul><li><b>html verwijderd</b></li>';
                            foreach ($report->deleted as &$msg) {

                                echo '<li>';
                                echo $msg;
                                echo '</li>';
                            }
                            echo '</li></ul>';
                        }

                        if (count($report->searchIndex) > 0) {

                            echo '<li><ul><li><b>toegevoegd aan zoekindex</b></li>';
                            foreach ($report->searchIndex as &$msg) {

                                echo '<li>';
                                echo $msg;
                                echo '</li>';
                            }
                            echo '</li></ul>';
                        }


                        if (count($report->removedFromIndex) > 0) {

                            echo '<li><ul><li><b>verwijderd uit zoekindex</b></li>';
                            foreach ($report->removedFromIndex as &$msg) {

                                echo '<li>';
                                echo $msg;
                                echo '</li>';
                            }
                            echo '</li></ul>';
                        }

                        if (count($report->datasets) > 0) {

                            echo '<li><ul>';
                            foreach ($report->datasets as &$msg) {

                                echo '<li>';
                                echo $msg;
                                echo '</li>';
                            }
                            echo '</li></ul>';
                        }

                        ?>
                    </ul>
                </div>
                <?php

            }
        }
    }

    add_action( 'admin_notices', 'sample_admin_notice__success' );