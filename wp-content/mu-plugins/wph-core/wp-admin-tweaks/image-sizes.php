<?php

// important for media library


if ( function_exists( 'add_theme_support' ) ) {

  add_theme_support( 'post-thumbnails' );
  add_image_size('thumb', 500, 500, true);
  add_image_size('klein', 768);
  add_image_size('medium', 1280);
  add_image_size('groot', 1750);

  // todo: fit this to media library only + remove originals
}
