<?php

// https://wordpress.org/plugins/json-api/other_notes/
// https://codex.wordpress.org/Plugin_API/Action_Reference
// http://localhost/wordpress/api/get_post/?post_id=6
// http://wordpress.stackexchange.com/questions/120996/add-action-only-on-post-publish-not-update

function saveContentController($post_id){

        $publish_controller = new Publish_Controller();

        $new_status = get_post_status($post_id); // get new status
        $old_status = get_post_meta($post_id, 'old_status', true); // get previous status

        // dit is belangrijk voor niet versturen emails
        if (metadata_exists('post', $post_id, 'old_status') == true) {
            update_post_meta($post_id, 'old_status', $new_status);
        } else {
            add_post_meta($post_id, 'old_status', $new_status, true); // update old status
        }

        if ($new_status === 'publish') {
            $publish_controller->saveContent($post_id);
        } elseif ($old_status === 'publish' && ($new_status === 'draft' || $new_status === 'pending')) {
            $publish_controller->deleteContent($post_id);
        }
}

function deleteContentController($post_id){

        $publish_controller = new Publish_Controller();
        $publish_controller->deleteContent($post_id);
}

// Saving posts
add_action('save_post', 'saveContentController', 20);
add_action('wp_trash_post', 'deleteContentController', 20);

