<?php

function set_custom_preview_link($link, $post) {
    $url = get_site_url(); // get base url
    return "$url/wp-content/mu-plugins/wph-core/preview/preview.php?id=$post->ID";
}

add_filter( 'preview_post_link', 'set_custom_preview_link', 10, 2 );
