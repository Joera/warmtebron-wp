<?php

// get array of term id's from array of term objects
function getTaxonomyIds($taxonomy_array) {
    $ids = array();
    foreach ( $taxonomy_array as $term ) {
        array_push($ids, $term->term_id);
    }
    return $ids;
}


function send_new_comment_notification($email,$commentID,$commentPostID) {

    $comment = get_comment($commentID);
    $post = get_post($commentPostID);

    setlocale(LC_ALL, 'nl_NL'); // set locale for date formatting in dutch
    $datestring = strftime("%e %B"); // set date string
    $from = json_decode(WPH_CONFIG)->subscription->from;
    $text = '';
    $replyTo = json_decode(WPH_CONFIG)->subscription->replyTo;
    $subject = json_decode(WPH_CONFIG)->subscription->subjects->new_comment . $post->post_title . '"';
    $html = '';

    $token = '';

    if ($post->post_type == 'post') {
        $post_url = WP_HOME . '/nieuws/' . explode("-", $post->post_date)[0] . '/' . $post->post_name; // set the url of the blog post
    } else {
        $post_url = WP_HOME . '/' . $post->post_name;
    }

    include(PLUGIN_FOLDER . 'wp-pages/email-templates/comment-notification.php');


    $mailgun = new Mailgun_Connector();
    $mailgun->send_email($from, $email, $subject, $text, $html, '', $replyTo);

}


function notify_editors_about_new_comment($emails,$commentID) {

    $redactiemail = json_decode(WPH_CONFIG)->subscription->replyTo;

    if (in_array($redactiemail, $emails)) {
        array_push($emails,'joeramulders@gmail.com');
    } else {
        array_push($emails,'joeramulders@gmail.com', $redactiemail);
    }

    $comment = get_comment($commentID);
    $postID = $comment->comment_post_ID;
    $post = get_post($postID);

    setlocale(LC_ALL, 'nl_NL'); // set locale for date formatting in dutch
    $datestring = strftime("%e %B"); // set date string
    $from = json_decode(WPH_CONFIG)->subscription->from;
    $text = '';
    $replyTo = json_decode(WPH_CONFIG)->subscription->replyTo;
    $subject = json_decode(WPH_CONFIG)->subscription->subjects->new_comment . $post->post_title . "'";
    $html = '';
    $token = '';
    $email = '';

    $post_url = get_permalink($postID);

    include(PLUGIN_FOLDER . 'wp-pages/email-templates/comment-notification-for-editors.php');

    foreach ( $emails as $email ) {
        $mailgun = new Mailgun_Connector();
        $mailgun->send_email($from, $email, $subject, $text, $html, '', $replyTo);
    }
}

//function do_direct_mail($post_id) {
//    $ch = curl_init(WP_HOME . '/wp-json/wp/v2/email/?method=direct&postID=' . $post_id);
//    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
//    $response = curl_exec($ch);
//    curl_close($ch);
//}

function direct_mail_hook($post_id) {

    $type = get_post_type($post_id); // get post type

    if ($type == 'post'){

        $new_status = get_post_status($post_id); // get new status
        $old_status = get_post_meta($post_id, 'old_status', true); // get previous status

        if ( $new_status == 'publish' && $old_status != 'publish') {
            $email_controller = new Email_Controller();
            $email_controller->directMail($post_id);
        }
    }
}


// add_action( 'direct_mail_action','do_direct_mail', 10, 3 );
add_action('acf/save_post', 'direct_mail_hook', 20, 3);

add_filter( 'comment_moderation_recipients', 'notify_editors_about_new_comment', 11, 2 );
add_filter( 'comment_notification_recipients', 'notify_editors_about_new_comment', 11, 2 );

// add_action( 'new_comment_notification', 'send_new_comment_notification', 10, 3 );
