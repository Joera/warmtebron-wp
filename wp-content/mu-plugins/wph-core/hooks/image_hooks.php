<?php


function add_file($attachment_id) {
    // if(wp_attachment_is_image($attachment_id)) { // check if file is image
        // set file path
        $path = get_attached_file($attachment_id);
        if (function_exists('curl_file_create')) { // php 5.6+
          $file = curl_file_create($path);
        } else { //
          $file = '@' . realpath($path);
        }

        // set post data that will be send
        $post = array(
          'UPLOADCARE_PUB_KEY' => UPLOADCARE_PUB_KEY,
          'UPLOADCARE_STORE' => '1',
          'file'=> $file
        );

        // make http call to upload file to uploadcare
        $url = 'https://upload.uploadcare.com/base/';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        // get response from uploadcare
        $response = curl_exec ($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        // close http call
        curl_close ($ch);



        // save uploadcare id
        $file_id = json_decode($body)->{'file'}; // id of file in the uploadcare cdn
        $meta_id = add_post_meta($attachment_id, 'external_id', $file_id, true); // add uploadcare id to metadata of image
    // }
}

function edit_image($attachment_id) {
    if(wp_attachment_is_image($attachment_id)) { // check if file is image
      // get uploadcare id of file
      $uploadcare_id = get_post_meta($attachment_id, 'external_id', true);

        delete_image($attachment_id); // delete old image
        delete_post_meta($attachment_id, 'external_id', $uploadcare_id); // remove old image up;oadcare id from database
        add_image($attachment_id); // upload new image
    }
}

function delete_image($attachment_id) {
    if(wp_attachment_is_image($attachment_id)) { // check if file is image
        // get uploadcare id of file
        $uploadcare_id = get_post_meta($attachment_id, 'external_id', true);

        // make http call to upload file to uploadcare
        $url = 'https://api.uploadcare.com/files/' . $uploadcare_id . '/' ;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Uploadcare.Simple fb5eed0e23e760701d46:8f6980a240a8215005ae'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Uploadcare.Simple ' . UPLOADCARE_PUB_KEY . ':' . UPLOADCARE_PRI_KEY));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // get response from uploadcare
        $response = curl_exec ($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        // close http call
        curl_close ($ch);
    }
}

//
add_filter( 'add_attachment', 'add_file', 10, 2 );
// add_filter( 'edit_attachment', 'edit_image', 10, 2 );
// add_filter( 'delete_attachment', 'delete_image', 10, 2 );
