<?php

function refresh_post($comment_id) {

 //   PC::debug($comment_id);

    $comment = get_comment($comment_id);
    $publish_controller = new Publish_Controller();
    $publish_controller->saveContent($comment->comment_post_ID);
}


add_action('edit_comment', 'refresh_post', 10, 2);
add_action('comment_post', 'refresh_post', 10, 2);
add_action('trash_comment', 'refresh_post', 10, 2);
add_action('untrash_comment', 'refresh_post', 10, 2);
add_action('deleted_comment', 'refresh_post', 10, 2);