<?php
defined( 'ABSPATH' ) or die( 'You can\'t access this file directly!');



class WPH_Emails {
    public function init() {
        $this->hooks();
        $this->email_controller = new Email_Controller();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_email_endpoint'));
    }

    public function register_email_endpoint() {
        register_rest_route( 'wp/v2', '/email/', array(
            'methods' => 'GET',
            'callback' => array($this, 'submit_email')
        ) );
    }

    public function submit_email($request) {
        $params = $request->get_params();
        nocache_headers();

        switch ($params['method']) {
            case 'direct':
                // check if subscription url parameter is valid
                if(isset($params['postID'])) {
                    $this->email_controller->directMail($params['postID']);
                }
                break;
            case 'weekly':

                break;
            case 'monthly':

                break;
            case 'comment';
                    // via hook only
                break;

            case 'contactform';
                    // straight on the endpoint
                break;
            case 'subscriber';
                // straight on the endpoint
                break;
        }

        return array('status' => 200, 'message' => __('Your email task has been submitted.', 'wph-user-comments'));


    }

}

$wph_emails = new WPH_Emails();
$wph_emails->init();