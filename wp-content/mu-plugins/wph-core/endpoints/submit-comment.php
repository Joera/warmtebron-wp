<?php
defined( 'ABSPATH' ) or die( 'You can\'t access this file directly!');


class WPH_User_Comments {
    public function init() {
        $this->hooks();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_submit_comment_endpoint'));
    }

    public function register_submit_comment_endpoint() {
        register_rest_route( 'wp/v2', '/submit_comment/', array(
            'methods' => 'POST',
            'callback' => array($this, 'insert_comment')
        ) );
    }

    public function insert_comment($request) {

        $params = $request->get_params();
        // Check if message is not empty
        if (!isset($params['message']) || empty($params['message']))
            return array('status' => 400, 'message' => __('Make sure to write a message.', 'wph-user-comments'));

        if (!isset($params['post_id']) || empty($params['post_id']))
            return array('status' => 400, 'message' => __('Something went wrong 1.', 'wph-user-comments'));

        $post_id = absint($params['post_id']);

        // @todo
        // Check if user wants to receive notifications + newsletter
        // if (isset($params['subscribe_to_newsletter']) && is_bool($params['subscribe_to_newsletter']))
        //     $args['subscribe_to_newsletter'] = $params['subscribe_to_newsletter'];
        
        // if (isset($params['get_notifications']) && is_bool($params['get_notifications']))
        //     $args['get_notifications'] = $params['get_notifications'];

        $args = array(
            'comment_post_ID' => false,
            'comment_parent' => 0,
            'comment_author' => 'Anoniem',
            'comment_author_email' => 'anoniem@anoniem.an',
            'comment_content' => '',
            'comment_date' => current_time('mysql'),
        );

        if (isset($params['post_id']) && !empty($params['post_id']))
            $args['comment_post_ID'] = absint($params['post_id']);

        if (isset($params['message']) && !empty($params['message']))
            $args['comment_content'] = sanitize_textarea_field(urldecode($params['message']));

        if (isset($params['email']) && !empty($params['email']))
            $args['comment_author_email'] = sanitize_email($params['email']);
        
        if (isset($params['name']) && !empty($params['name']))
            $args['comment_author'] = sanitize_textarea_field(urldecode($params['name']))   ;

        if (isset($params['comment_parent']) && !empty($params['comment_parent']))
            $args['comment_parent'] = $params['comment_parent'];

        $editor = get_user_by( 'email', $params['email'] );

        if ($editor != false) {
            $args['user_id'] = $editor->ID;
        }

        $insert_comment = wp_insert_comment($args);

        if (is_wp_error($insert_comment)) {
            return array('status' => 400, 'message' => __('Something went wrong 2.', 'wph-user-comments'));
        } else {
            add_comment_meta($insert_comment, 'rating', 0 );
            update_post_meta($post_id, '_last_comment', current_time('timestamp'));
            wp_update_post(array('ID' => $post_id));
            wp_notify_postauthor($insert_comment);
        }

        return array('status' => 200, 'message' => __('Your comment has been submitted.', 'wph-user-comments'));
    }
}

$wph_user_comments = new WPH_User_Comments();
$wph_user_comments->init();