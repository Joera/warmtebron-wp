<?php

class WPH_Comment_ratings {


    public function init() {
        $this->hooks();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_get_comment_rating_endpoint'));
        add_action('rest_api_init', array($this, 'register_submit_comment_rating_endpoint'));
    }


    public function register_get_comment_rating_endpoint() {
        register_rest_route( 'wp/v2', '/comment_rating/', array(
            'methods' => 'GET',
            'callback' => array($this, 'get_comment_rating')
        ) );
    }

    public function register_submit_comment_rating_endpoint() {
        register_rest_route( 'wp/v2', '/comment_rating/', array(
            'methods' => 'POST',
            'callback' => array($this, 'submit_comment_rating')
        ) );
    }

    public function get_comment_rating($request) {


    }

    public function submit_comment_rating($request) {

        $comment_id = $request['comment_id'];
        $current_rating = get_comment_meta($comment_id, 'rating');
        $new_rating = (int)$current_rating[0] + 1;
        update_comment_meta($comment_id, 'rating',$new_rating);

        $comment = get_comment($comment_id);

        $publish_controller = new Publish_Controller();
        $publish_controller->saveContent($comment->comment_post_ID);

        return new WP_REST_Response($new_rating,200);

    }

}

$wph_comment_ratings = new WPH_Comment_ratings();
$wph_comment_ratings->init();


