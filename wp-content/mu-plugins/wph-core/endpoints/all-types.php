<?php

    function get_all_content($request) {
        // rest_ensure_response() wraps the data we want to return into a WP_REST_Response, and ensures it will be properly returned.
        global $post;
        // Get all types except these
        $types_not_needed = array(
            'customize_changeset',
            'attachment',
            'revision',
            'nav_menu_item',
            'custom_css',
            'acf',
            'subscriber',
            'feed',
            'oembed_cache',
            'acf-field-group',
            'acf-field',
            'user_request'
        );



        $post_types = get_post_types();
        foreach ($types_not_needed as $value)
            unset($post_types[$value]);

        $size = 100;

        $posts = get_posts(array(
            'post_type' => $post_types,
            'posts_per_page' => $size,
            'offset' => $size * intval($request['page'])
        ));

        $totalItems = wp_count_posts($post_types);

        $controllers = array();
        foreach ( $post_types as $post_type) {
            $controllers[$post_type] = new WP_REST_Posts_Controller($post_type);
        }

        $items = array();

        foreach ( $posts as $post ) {
            $data    = $controllers[$post->post_type]->prepare_item_for_response( $post, $request );
            $data->data = apply_filters( 'transform-post-object', $data->data);
            $items[] = $controllers[$post->post_type]->prepare_response_for_collection( $data );
        }

        $response =  new WP_REST_Response($items,200);

        $links = array();

        if(count($items) == $size) {
            $links['next'] = array( 'href' => WP_HOME . '/wp-json/wp/v2/all?page=' . (intval($request['page']) + 1));
        }

        $response->add_links($links);

        return $response;
    }
    function register_all_types_route() {
        register_rest_route( 'wp/v2', '/all', array(
            'methods'  => WP_REST_Server::READABLE,
            'callback' => 'get_all_content',
        ) );
    }

    add_action( 'rest_api_init', 'register_all_types_route' );
