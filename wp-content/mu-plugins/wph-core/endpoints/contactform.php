<?php
defined( 'ABSPATH' ) or die( 'You can\'t access this file directly!');



class WPH_ContactForm {
    public function init() {
        $this->hooks();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_contactform_endpoint'));
    }

    public function register_contactform_endpoint() {
        register_rest_route( 'wp/v2', '/submit_contactform/', array(
            'methods' => 'POST',
            'callback' => array($this, 'submit_contactform')
        ) );
    }

    public function submit_contactform($request) {
        $params = $request->get_params();
        nocache_headers();

        $name = $params['name'];
        $email = $params['email'];
        $text = $params['text'];
        $subject = json_decode(WPH_CONFIG)->subscription->subjects->contact_form;
        $from = json_decode(WPH_CONFIG)->subscription->from;
        $to = json_decode(WPH_CONFIG)->subscription->to;
        // $to = 'joeramulders@gmail.com';

        $html = '';

        include(PLUGIN_FOLDER . 'wp-pages/email-templates/contactform.php');

        $headers = array('Content-Type: text/html; charset=UTF-8');
        $mailgun = new Mailgun_Connector();
        $mailgun->send_email($from,$to,$subject,$text,$html,$email);

        return array('status' => 200, 'message' => __('Your contactform has been submitted.', 'wph-user-comments'));
    }
}

$wph_contactform = new WPH_ContactForm();
$wph_contactform->init();