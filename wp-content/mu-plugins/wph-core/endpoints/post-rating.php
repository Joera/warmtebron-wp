<?php

    class WPH_Submit_Post_ratings {


        public function init() {
            $this->hooks();
        }

        public function hooks() {
            add_action('rest_api_init', array($this, 'register_get_post_rating_endpoint'));
            add_action('rest_api_init', array($this, 'register_submit_post_rating_endpoint'));
        }


        public function register_get_post_rating_endpoint() {
            register_rest_route( 'wp/v2', '/post_rating/', array(
                'methods' => 'GET',
                'callback' => array($this, 'get_post_rating')
            ) );
        }

        public function register_submit_post_rating_endpoint() {
            register_rest_route( 'wp/v2', '/post_rating/', array(
                'methods' => 'POST',
                'callback' => array($this, 'submit_post_rating')
            ) );
        }

        public function get_post_rating($request) {

            $params = $request->get_params();
            $postID = absint($params['post_ID']);

            if ( get_post_status($postID)) {

                $rating =  new stdClass();
                $positive_count = get_field('positive-count', $postID);
                $negative_count = get_field('negative-count',$postID);
                $total_count = $positive_count + $negative_count;
                if ($total_count > 0 ) {
                    $percentage = round(($positive_count / $total_count) * 100);
                }  else {
                    $percentage = 0;
                }
                $data = array(

                    'positive_count' => absint($positive_count),
                    'negative_count' => absint($negative_count),
                    'total_count' => absint($total_count),
                    'percentage' => absint($percentage)
                );

                return new WP_REST_Response($data,200);

            } else {
                return 'no post found with this ID';
            }
        }

        public function submit_post_rating($request) {

            $params = $request->get_params();
            $postID = absint($params['post_ID']);

            if ( get_post_status($postID)) {

                if ($request['value'] === 'positive') {

                    $field = 'positive-count';
                    $current_value = get_field($field, $postID);
                    $new_value = (int)$current_value + 1;
                    update_field($field, $new_value, $postID);

                } else if ($request['value'] === 'negative') {

                    $field = 'negative-count';
                    $current_value = get_field($field, $postID);
                    $new_value = (int)$current_value + 1;
                    update_field($field, $new_value, $postID);
                }

                $publish_controller = new Publish_Controller();
                return $publish_controller->saveContent($postID);
            }
        }
    }

$wph_post_ratings = new WPH_Submit_Post_ratings();
$wph_post_ratings->init();



