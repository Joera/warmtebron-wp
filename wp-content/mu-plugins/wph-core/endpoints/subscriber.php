<?php

class WPH_Subscriber_Endpoint {

    public function init() {
        $this->hooks();
    }

    public function hooks() {
        add_action('rest_api_init', array($this, 'register_get_subscriber_endpoint'));
        add_action('rest_api_init', array($this, 'register_add_subscriber_endpoint'));
        add_action('rest_api_init', array($this, 'register_update_subscriber_endpoint'));
    }

    public function register_get_subscriber_endpoint() {
        register_rest_route( 'wp/v2', '/subscriber/', array(
            'methods' => 'GET',
            'callback' => array($this, 'get_subscriber')
        ) );
    }

    public function register_add_subscriber_endpoint() {
        register_rest_route( 'wp/v2', '/subscriber/', array(
            'methods' => 'POST',
            'callback' => array($this, 'add_subscriber')
        ) );
    }

    public function register_update_subscriber_endpoint() {
        register_rest_route( 'wp/v2', '/subscriber/', array(
            'methods' => 'PUT',
            'callback' => array($this, 'update_subscriber')
        ) );
    }

    public function get_subscriber($request) {

        $params = $request->get_params();

        if (!isset($params['token']) || empty($params['token']))
            return array('status' => 400, 'message' => __('Please include token', 'wph-calendar'));

        $token = $params['token'];

        // get subscriber from database
        $args = array(
            'post_type'     => 'subscriber',
            'post_status'   => 'publish',
            'meta_query' => array(
                array(
                    'key' => 'token',
                    'value' => $token,
                    'compare' => 'LIKE'
                )
            )
        );

        $query = new WP_Query($args);
        $post = $query->post; // get post data of subscriber

        if ($post) {

            $controller = new WP_REST_Posts_Controller('subscriber');
            $data = $controller->prepare_item_for_response( $post, $request);
            $data->data = apply_filters( 'transform-post-object', $data->data);
            $items[] = $controller->prepare_response_for_collection($data);
            return new WP_REST_Response($items,200);

        } else {
            return array('status' => 400, 'message' => __('Subscriber not found', 'wph-calendar'));
        }
    }

    public function add_subscriber($request) {

        $params = $request->get_params();
        $email_Controller = new Email_Controller();

        // Check if message is not empty
        if (!isset($params['email']) || empty($params['email']))
            return array('status' => 400, 'message' => __('Make sure to include an email.', 'wph-calendar'));

        if (!isset($params['subscription']) || empty($params['subscription']))
            return array('status' => 400, 'message' => __('Make sure to include a subscription type.', 'wph-calendar'));

        $args = array(
            'post_type' => 'subscriber',
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key' => 'email',
                    'value' =>  $params['email'],
                    'compare' => 'LIKE'
                )
            )
        );
        // check if email already in use
        $query = new WP_Query($args);

        if($query->post_count > 0) {
            $post_id = $query->posts[0]->ID;
            $token = get_post_meta($post_id,'token')[0];
            $email_Controller->subscription_confirmation($_REQUEST['email'], $token, get_post_meta($post_id,'language')[0]);
            // Already subscribed
            return array('status' => 400, 'message' => __('This email is allready subscribed', 'wph-calendar'));
        } else {

            // new subscriber data
            $new_subscriber = array(
                'post_title' => $params['email'],
                'post_content' => '',
                'post_status' => 'publish',
                'post_type' => 'subscriber',
            );

            // generate token
            $token = uniqid().uniqid().uniqid();
            // Add new subscriber
            $postID = wp_insert_post($new_subscriber);

            if (is_wp_error($postID)) {
                $html = print_r($args, true) . "<br/><br/>" . print_r($params['email'], true);
                wp_mail('joeramulders@gmail.com', 'fout bij aanmelden abbonnee warmtebron', $html);
                return array('status' => 400, 'message' => __('Something went wrong.', 'wph-calendar'));
            } else {

                $custom_fields = array(
                    'email' => $params['email'],
                    'subscription' => $params['subscription'],
                    'language' => $params['language'],
                    'token' => $token
                );

                foreach ($custom_fields as $key => $value) {
                    update_field($key, $value, $postID);
                }

                $email_Controller->subscription_confirmation($_REQUEST['email'], $token, $params['language']);
                return array('status' => 200, 'message' => __('Your subscription has been submitted.', 'wph-calendar'));
            }
        }

    }

    public function update_subscriber($request) {

        $params = $request->get_params();

        if (!isset($params['id']) || empty($params['id']))
            return array('status' => 400, 'message' => __('Make sure to include an id value.', 'wph-calendar'));
        // Check if message is not empty
        if (!isset($params['email']) || empty($params['email']))
            return array('status' => 400, 'message' => __('Make sure to include an email.', 'wph-calendar'));

        if (!isset($params['subscription']) || empty($params['subscription']))
            return array('status' => 400, 'message' => __('Make sure to include a subscription type.', 'wph-calendar'));

        try {
            update_field( 'subscription', $params['subscription'], $params['id'] );
            update_field( 'language', $params['language'], $params['id'] );

//            wp_set_object_terms($params['id'], explode(",", $params['themes']), 'category');
//            wp_set_object_terms($params['id'], explode(",", $params['constructionprojects']), 'construction-project');

            return array('status' => 200, 'message' => __('Your subscription has been changed.', 'wph-calendar'));

        } catch (Exception $e) {
            return array('status' => 400, 'message' => __('Something went wrong.', 'wph-calendar'));
        }
    }
}

$wph_subscriber_endpoint = new WPH_Subscriber_Endpoint();
$wph_subscriber_endpoint->init();



