<?php


class Publish_Controller {


    function __construct() {
        $this->report_services = new Report_Services();
    }

  function update($postObject) {

      $postObject = apply_filters( 'log-post-object', $postObject);

      $ch = curl_init(API_URL_FRONTEND . '/sg-api/content' );
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Authorization: ' . API_URL_FRONTEND_AUTH_KEY));
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postObject));
      $response = curl_exec($ch);
      $error = curl_error($ch);
      curl_close($ch);

      $this->report($response,$postObject);
  }


  function delete($postObject) {
      $ch = curl_init(API_URL_FRONTEND . '/sg-api/content' );
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Authorization: ' . API_URL_FRONTEND_AUTH_KEY));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postObject));
      $response = curl_exec($ch);
      curl_close($ch);
  }

  function report($response,$postObject) {

    $report = json_decode($response);
    set_transient( '_cms_report', $report, 30);
    $this->report_services->byEmail($report,$postObject);
    }

  // triggered on save action
  function saveContent( $post_id ) {

      $post = get_post($post_id);

      $controller = new WP_REST_Posts_Controller($post->post_type);
      $request = new WP_REST_Request();

      $WPResponse = $controller->prepare_item_for_response( $post, $request );
      $postData = apply_filters( 'transform-post-object', $WPResponse->data);
      $permission = apply_filters( 'publish-permissions', $post_id);


      $new_status = get_post_status($post_id); // get new status
      $old_status = get_post_meta($post_id, 'old_status', true); // get previous status

      // dit is belangrijk voor niet versturen emails
      if (metadata_exists('post', $post_id, 'old_status') == true) {
          update_post_meta($post_id, 'old_status', $new_status);
      } else {
          add_post_meta($post_id, 'old_status', $new_status, true); // update old status
      }

      if ($post->post_status === 'draft') {
          $this->delete($postData);
          
      } else if($permission) {
          $this->update($postData);

          // whattabout delete?
      }
  }

  // triggered on trash post
  function deleteContent($post_id) {

    $type = get_post_type($post_id); // get post type

    $controller = new WP_REST_Posts_Controller($type);
    $request = new WP_REST_Request();

    if ($type !== 'acf' && $type !== 'feed') {
      $post = get_post($post_id);
      $status = get_post_status($post_id); // get new status
      $postObject = $controller->prepare_item_for_response( $post, $request );
      $this->delete($type, $postObject);
    }
  }
}
