<?php


class Email_Controller {

    function init() {
//        $this->subscriber_services = new Subscriber_Services();
//        $this->email_services = new Email_Services();
    }

    function directMail($postID) {

        $subscriber_services = new Subscriber_Services();
        $email_services = new Email_Services();

        // this is the id for a job of email tasks.
        $mailingID = uniqid().uniqid().uniqid();

        // get content
        $post = get_post($postID);
        $post_url = get_permalink($postID);    // WP_HOME . '/blog/' . explode("-", $post->post_date)[0] . '/' . $post->post_name; // set the url of the blog post
        $lan = 'nl';

        $subject = 'Warmtebron Utrecht | ' . $post->post_title;
        $introductiontext = 'Dit is jouw update over Warmtebron Utrecht.';
        $text = $post_url;
        setlocale(LC_ALL, 'nl_NL'); // set locale for date formatting in dutch
        $datestring = strftime("%e %B"); // set date string
        $from = json_decode(WPH_CONFIG)->subscription->from;
        $replyTo = json_decode(WPH_CONFIG)->subscription->replyTo;
        $html = '';

        $main_image = get_field('main_image',$postID);

        if( $main_image && $main_image['image'] && $main_image['image']['ID']) {
            $imageObject = create_image_object_by_id($main_image['image']['ID']);
            $image_url = 'https://ucarecdn.com/' . $imageObject->external_id . '/-/scale_crop/560x373/center/' . $imageObject->filename;
        } else {
            $image_url = '';
        }

        $subscribers = $subscriber_services->findSubscribersByPost($postID,'direct','nl');

        // loop subscribers
        foreach ($subscribers as $subscriber) {
            $email = get_post_meta($subscriber->ID, 'email')[0]; // get email address of subscriber
            $token = get_post_meta($subscriber->ID, 'token')[0];

            include(PLUGIN_FOLDER . 'wp-pages/email-templates/post-notification.php');

            $email_services->setTask($mailingID,$from,$email,$subject,$text,$html,$replyTo);
        }

        $email_services->setJob($mailingID,$text);
    }


    function rsvp_confirmation( $params ) {


    }

    function subscription_confirmation($email, $token, $lan) {

        // set current date string
        setlocale(LC_ALL, 'nl_NL');
        $datestring = strftime("%e %B");

        $from = json_decode(WPH_CONFIG)->subscription->from;

//        if($lan == 'en') {
//            $subject = json_decode(WPH_CONFIG)->subscription->subjects->subscription_confirmation_english;
//        } else {
//
//        }

        $subject = json_decode(WPH_CONFIG)->subscription->subjects->subscription_confirmation;

        $text = '';
        $replyTo = json_decode(WPH_CONFIG)->subscription->replyTo;
        $html = '';
        $post_url = WP_HOME;
        $post = null;

        include(PLUGIN_FOLDER . 'wp-pages/email-templates/subscription_confirmation.php');

        $headers = array('Content-Type: text/html; charset=UTF-8');
        $mailgun = new Mailgun_Connector();
        $mailgun->send_email($from, $email, $subject, $text, $html, '',$replyTo);
    }

    function newsletter($email, $token, $posts,$introductiontext) {

    }
}
