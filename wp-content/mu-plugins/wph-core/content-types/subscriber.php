<?php

function register_subscriber() {

    // register theme custom post type
    $args = array(
        'public' => true,
        'label'  => 'Abonnee',
        'labels' => array(
            'add_new_item' => 'Abonnees toevoegen',
            'new_item' => 'Nieuwe abonnee',
            'view_item' => 'Bekijk abonnees',
            'view_items' => 'Bekijk abonnees'
        ),
        "show_in_rest" => true,
        "rest_base" => "subscriber",
        "has_archive" => false,
        "show_in_menu" => true,
        "menu_position" => 5,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "subscriber", "with_front" => false ),
        "query_var" => true,
        "supports" => array("title"),
        'taxonomies' => array('category')
    );
    register_post_type( 'subscriber', $args );
}

add_action( 'init', 'register_subscriber' );