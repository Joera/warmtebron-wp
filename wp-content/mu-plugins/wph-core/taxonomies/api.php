<?php

add_action( 'init', 'register_taxonomies' );

function register_taxonomies() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->taxonomies,
        'taxonomies',
        array(
            'get_callback'    => 'collect_taxonomies',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_categories_as_objects($post_id) {

    $post_categories = wp_get_post_categories( $post_id);
    $cats = array();

    foreach($post_categories as $catID){

        $cat = get_category( $catID );
        if( $cat->cat_ID != '1') {
            $cats[] = array('name' => $cat->name, 'slug' => $cat->slug, 'id' => $cat->cat_ID);
        }
    }
    return $cats;
}

function get_tags_as_objects($post_id) {

    $post_tags = wp_get_post_tags( $post_id );
    $tags = array();

    foreach($post_tags as $t){
        $tag = get_tag( $t );
        $tags[] = array( 'name' => $tag->name, 'slug' => $tag->slug, 'id' => $tag->term_id );
    }
    return $tags;
}

function collect_taxonomies( $object, $field_name, $request ){

    $taxonomies = array(
        'categories' => get_categories_as_objects($object['id']),
        'tags' => get_tags_as_objects($object['id'])
    );

    foreach(glob(ABSPATH . 'wp-content/plugins/wp-pages/taxonomies/*/api.php') as $file){
        include $file;
    }

    return $taxonomies;
}


