<?php

/*
Persistance for emails. Handles all database operations
*/

class Email_Store {

  // add email to email store
  function add_email($from, $to, $subject, $text, $html, $mailgun_id, $mailgun_message, $mailingID) {
    global $wpdb;

    // save email to database
  	$table_name = $wpdb->prefix . "send_email"; // set the table name
  	$res = $wpdb->insert(
  		$table_name,
  		array(
  			'time' => current_time( 'mysql' ),
  			'from_address' => $from,
  			'to_address' => $to,
  			'subject' => $subject,
  			'text' => $text,
//            'html' => $html,
  			'mailgun_id' => $mailgun_id,
  			'mailgun_message' => $mailgun_message,
            'mailing_id' => $mailingID,
        	'status' => 'Queued',
  		)
  	);
    return $res;
  }


  //get all emails send to provided email address
  function get_emails($email) {
    global $wpdb;

    $table_name = $wpdb->prefix . "send_email"; // set the table name
		$send_emails = $wpdb->get_results(
			"
			SELECT from_address, to_address, subject, time, status
			FROM $table_name
			WHERE to_address LIKE '%$email%'
			ORDER BY time
			"
		);
    return $send_emails;
  }


  // update the mail status
  function update_email_status($mailgun_id, $status) {
    global $wpdb;

    $table_name = $wpdb->prefix . "send_email"; // set the table name
    $res = $wpdb->update(
    	$table_name,
    	array(
    		'status' => $status
    	),
    	array( 'mailgun_id' => $mailgun_id )
    );
    return $res;
  }



}
?>
