<?php

class Email_Services {

    function setTask($mailingID,$from,$email,$subject,$text,$html,$replyTo) {

        global $wpdb;
        // save email tasks to database
        $table_name = $wpdb->prefix . "email_tasks"; // set the table name
        $res = $wpdb->insert(
            $table_name,
            array(
                'mailing_id' => $mailingID,
                'time' => current_time( 'mysql' ),
                'from' => $from,
                'to' => $email,
                'subject' => $subject,
                'text' => $text,
                'html' => $html,
                'cc' => '',
                'reply_to' => $replyTo,
                'status' => 'unsend'
            )
        );
    }

    function setJob($mailingID,$text) {

        global $wpdb;
        $job_table = $wpdb->prefix . "email_jobs";
        $res_job = $wpdb->insert(
            $job_table,
            array(
                'datetime' => current_time( 'mysql' ),
                'mailing_id' => $mailingID,
                'type' => 'direct',
                'post_name' => $text,
                'status' => 'unsend'
            )
        );
    }
}