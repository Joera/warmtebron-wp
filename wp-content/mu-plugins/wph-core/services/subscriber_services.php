<?php

class Subscriber_Services {

    public function getSubscriberPosts($subscriber_id, $subscription, $language) {
        // getTaxonomyIds is declared in direct_mail_hook.php
        $subscriber_categories_ids = getTaxonomyIds(wp_get_post_terms($subscriber_id, 'category')); // id's of the categories the subscriber subscribed to
        $subscriber_construction_projects_ids = getTaxonomyIds(wp_get_post_terms($subscriber_id, 'construction-project')); // id's of the construction-projects the subscriber subscribed to
        $all_categories_ids = getTaxonomyIds(get_terms( array('taxonomy' => 'category','hide_empty' => false))); // id's of all taxonomy categories
        $all_construction_projects_ids = getTaxonomyIds(get_terms( array('taxonomy' => 'construction-project','hide_empty' => false))); // id's of all taxonomy construction-projects

        // set date query parameter
        $date_query_parameter = '1 week ago'; // '1 week ago';
        if($subscription == 'monthly') {
            $date_query_parameter = '1 month ago';
        }

        // set query arguments for getting posts that will be send in the newsletter
        if(count($subscriber_categories_ids) == 0 && count($subscriber_construction_projects_ids) == 0 ) {
            // get all posts of last period
            $args = array(
                'post_type' => array( 'post'),
                'post_status' => 'publish',
                'posts_per_page'=> -1,
                'orderby' => 'date',
                'order' => 'DESC',
                'date_query' => array(
                    array(
                        'after' => $date_query_parameter
                    )
                )
            );
        } else {
            // get posts that subscriber subscribed to of last period
            $args = array(
                'post_type' => array( 'post'),
                'post_status' => 'publish',
                'orderby' => 'date',
                'order' => 'DESC',
                'date_query' => array(
                    array(
                        'after' => $date_query_parameter
                    )
                ),
                'tax_query' => array(
                    'relation' => 'OR',
                    array( // subscribed to category that post also has
                        'taxonomy' => 'category',
                        'field'    => 'term_id',
                        'terms' => $subscriber_categories_ids,
                        'operator' => 'IN',
                    ),
                    array( // subscribed to construction-project that post also has
                        'taxonomy' => 'construction-project',
                        'field'    => 'term_id',
                        'terms' => $subscriber_construction_projects_ids,
                        'operator' => 'IN',
                    ),
                )
            );
        }

        // iedereen krijgt de tot blog gepromoveerde evenementen
        $activities = array(
            'post_type' => 'activity',
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key'     => 'render_as_blog',
                    'value'   => 'blog',
                    'compare' => 'LIKE'
                ),
            ),
            'date_query' => array(
                array(
                    'after' => $date_query_parameter
                )
            ),
            'orderby' => 'date',
            'order' => 'DESC'
        );

        $queryCurrentActivities = new WP_Query($activities);

        if (!isset($language)) {
            $language = 'nl';
        }

        // switch language
        global $sitepress;
        $sitepress->switch_lang($language);
        // get posts for the newsletter
        $queryPosts = new WP_Query($args);


        return array_merge($queryCurrentActivities->posts,$queryPosts->posts);
    }

    public function findSubscribersBySubscription($method,$batch) {

        $args = array(
            'post_type'     => 'contact',
            'post_status'   => 'publish',
            'posts_per_page'=> -1,
            'post__in' => [33115],  // ,3375,2854
            'meta_query' => array(
                array(
                    'key' => 'contact_subscription',
                    'value' => $method,
                    'compare' => 'LIKE'
                )
            )
        );

        $query = new WP_Query($args);
        return $query->posts;
    }


    public function findSubscribersByPost ($postID,$type, $lan) {

        if ($lan == 'en') {

            $args = array(
                'post_type' => 'subscriber',
                'post_status' => 'publish',
//                'post__in' => [13128],
                'posts_per_page' => -1,
                'meta_query' => array(
                    array(
                        'key' => 'subscription',
                        'value' => 'direct',
                        'compare' => 'LIKE'
                    ),
                    array(
                        'key' => 'language',
                        'value' => 'en',
                        'compare' => 'LIKE'
                    )
                )
            );
        } else {

            $args = array(
                'post_type' => 'subscriber',
                'post_status' => 'publish',
//                'post__in' => [13128],
                'posts_per_page' => -1,
                'meta_query' => array(
                    'relation' => 'AND',
                    array (
                        array(
                            'key' => 'subscription',
                            'value' => 'direct',
                            'compare' => 'LIKE'
                        ),
                        'relation' => 'OR', array (
                        array(
                            'key' => 'language',
                            'compare' => 'NOT EXISTS'
                        ),
                        array(
                            'key' => 'language',
                            'value' => 'nl',
                            'compare' => 'LIKE'
                        )
                    )
                    )
                )
            );

        }

        $query = new WP_Query($args);
        return $query->posts;
    }
}
