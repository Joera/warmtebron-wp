<?php

    require_once("../../../../wp-load.php"); // load wordpress libs to enable the wordpress functions in this document


    function getUnCompletedJobs() {

        global $wpdb;
        $table_name = $wpdb->prefix . "email_jobs";
        $jobs = $wpdb->get_results(
           " 
                    SELECT * 
                    FROM $table_name a
                    WHERE a.status = 'unsend'
        ");

        return $jobs;
    }

    function getUnsendTasks($mailingID) {
        global $wpdb;
        $table_name = $wpdb->prefix . "email_tasks";
        $tasks = $wpdb->get_results(
            $wpdb->prepare( " 
                    SELECT * 
                    FROM $table_name a
                    WHERE a.status = 'unsend'
                    AND a.mailing_id = %s
                    LIMIT 50",
                $mailingID
            )
        );
        return $tasks;
    }

    function updateTask($task,$mailgun_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . "email_tasks";
        $wpdb->update(
            $table_name,
            array(
                'status' => 'queued',
                'mailgun_id' => $mailgun_id // string// string
            ),
            array( 'mailing_id' => $task->mailing_id,
                'to' => $task->to
            ),
            array(),
            array()
        );

        return;
    }

    function updateJob($mailingID) {

        global $wpdb;
        $table_name = $wpdb->prefix . "email_jobs";
        $wpdb->update(
            $table_name,
            array(
                'status' => 'done'	// string
            ),
                array( 'mailing_id' => $mailingID,
            ),
            array(),
            array()
        );

        return;
    }


    $queue = array();
    $openJobs = getUnCompletedJobs();

    foreach ($openJobs as $openJob) {
        $openTasks = getUnsendTasks($openJob->mailing_id);
        if($openTasks && count($openTasks) > 0) {
            $queue = array_merge($queue,$openTasks);
        }
    }

    if (count($queue) > 0) {
        $queue = array_slice($queue,0,250);
    }

    $count = 0;
    foreach ($queue as $queueItem) {

        $mailgun = new Mailgun_Connector();
        $response = $mailgun->send_email($queueItem->from, $queueItem->to, $queueItem->subject, $queueItem->text, $queueItem->html, $queueItem->cc, $queueItem->reply_to);

        if($response) {
            updateTask($queueItem, $response->id);
            $count++;
        }

    }

    foreach ($openJobs as $openJob) {
        $openTasks = getUnsendTasks($openJob->mailing_id);
        if(!$openTasks || count($openTasks) < 1) {
            updateJob($openJob->mailing_id);
        }
    }


    wp_send_json( $count, 200 );
