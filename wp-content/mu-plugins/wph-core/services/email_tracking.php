<?php

require_once("../../../../wp-load.php"); // load wordpress libs to enable the wordpress functions in this document


function verify($apiKey, $token, $timestamp, $signature)
{
    // check if the timestamp is fresh
    if (abs(time() - $timestamp) > 15) {
        return false;
    }

    // returns true if signature is valid
    return hash_hmac('sha256', $timestamp . $token, $apiKey) === $signature;
}

function updateTask($event) {
    global $wpdb;
    $table_name = $wpdb->prefix . "email_tasks";
    $wpdb->update(
        $table_name,
        array(
            'mailgun_id' => '<' . $event['message']['headers']['message-id'] . '>'
        ),
        array(
            'raw' => JSON.stringify($event),
            'status' => $event['event']
        ),
        array(),
        array()
    );

    return;
}


$_POST = json_decode(file_get_contents('php://input'), true);

if(!verify(MAILGUN_KEY,$_POST['signature']['token'],$_POST['signature']['timestamp'],$_POST['signature']['signature'])) {
    wp_send_json( $res, 302 );
};

$id = $_POST['event-data']['id'];
$email = $_POST['event-data']['recipient'];
$status = $_POST['event-data']['event'];

updateTask($_POST['event-data']);


$res = new stdClass();

wp_send_json( $email, 200 );