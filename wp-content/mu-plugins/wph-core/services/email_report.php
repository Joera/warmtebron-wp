<?php

    require_once("../../../../wp-load.php"); // load wordpress libs to enable the wordpress functions in this document


    function getUnCompletedJobs() {

        global $wpdb;
        $table_name = $wpdb->prefix . "email_jobs";
        $jobs = $wpdb->get_results(
           " 
                    SELECT * 
                    FROM $table_name a
                    WHERE a.status = 'unsend'
        ");

        return $jobs;
    }

    function getUnsendTasks($mailingID) {
        global $wpdb;
        $table_name = $wpdb->prefix . "email_tasks";
        $tasks = $wpdb->get_results(
            $wpdb->prepare( " 
                    SELECT * 
                    FROM $table_name a
                    WHERE a.status = 'unsend'
                    AND a.mailing_id = %s
                    LIMIT 250",
                $mailingID
            )
        );
        return $tasks;
    }

    function getTasks($mailingID) {
        global $wpdb;
        $table_name = $wpdb->prefix . "email_tasks";
        $tasks = $wpdb->get_results(
            $wpdb->prepare( " 
                        SELECT * 
                        FROM $table_name a
                        WHERE a.mailing_id = %s",
                $mailingID
            )
        );
        return $tasks;
    }

    function updateTask($task,$mailgun_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . "email_tasks";
        $wpdb->update(
            $table_name,
            array(
                'status' => 'queued',
                'mailgun_id' => $mailgun_id // string
            ),
            array( 'mailing_id' => $task->mailing_id,
                'to' => $task->to
            ),
            array(),
            array()
        );
    
        return;
    }

    function updateJob($mailingID) {

        global $wpdb;
        $table_name = $wpdb->prefix . "email_jobs";
        $wpdb->update(
            $table_name,
            array(
                'status' => 'done'	// string
            ),
                array( 'mailing_id' => $mailingID,
            ),
            array(),
            array()
        );

        return;

    }

    if($_POST['mailingID']) {
        $mailing_id = $_POST['mailingID'];
    } else {
        $mailing_id = '5cf9216352d5a5cf9216352d5c5cf9216352d5d';
    }

    $tasks = getTasks($mailing_id);

    usort($tasks, function($a, $b) {
        return $a->to > $b->to ? 1 : -1;
    });

    foreach ($tasks as $task) {

        echo $task->to . ' - ' . $task->status . '<br/>';
    }

