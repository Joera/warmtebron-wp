<?php

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'appreciation',
		'title' => 'Waardering',
		'fields' => array (
			array (
				'key' => 'field_58d182e55d35f',
				'label' => 'Nuttig',
				'name' => 'positive-count',
				'type' => 'text',
				'default_value' => '0',
                'readonly'=> 1,
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => ''
			),
            array (
                'key' => 'field_58d182e55d35c',
                'label' => 'Niet nuttig',
                'name' => 'negative-count',
                'type' => 'text',
                'default_value' => '0',
                'readonly'=> 1,
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => ''
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'user_type',
                    'operator' => '==',
                    'value' => 'administrator',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                    'order_no' => 1,
                    'group_no' => 0,
                ),
            )
        ),
		'options' => array (
			'position' => 'side',
			'layout' => 'metabox',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}