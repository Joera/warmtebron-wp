<?php

add_action( 'init', 'register_interaction' );

function register_interaction() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->interaction,
        'interaction',
        array(
            'get_callback'    => 'get_interaction',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function create_comment($wp_comment) {

    $comment = new stdClass();
    $content = apply_filters('comment_text', $wp_comment->comment_content);

    $comment->id = (int) $wp_comment->comment_ID;
    $comment->name = $wp_comment->comment_author;
    $comment->url = $wp_comment->comment_author_url;
    $comment->date = $wp_comment->comment_date; // date($date_format, strtotime($wp_comment->comment_date));
    $comment->content = $content;
    $comment->parent = (int) $wp_comment->comment_parent;
    $comment->rating = get_comment_meta( $wp_comment->comment_ID, 'rating', true );

    if($wp_comment->user_id != 0) {
        $author = get_userdata($wp_comment->user_id);
        $organisation = get_user_meta($wp_comment->user_id,'organisation');

//        if(isset($organisation) && !empty($organisation) && count($organisation) > 0) {
//            $comment->organisation = $organisation[0];
//            $comment->name = $author->first_name . ' | ' . $organisation[0];
//        } else {
            $comment->name = $author->first_name . ' ' . $author->last_name;
//        }
        if(get_field('thumbnail',"user_" . $wp_comment->user_id)) {
            $thumbnail = get_field('thumbnail', "user_" . $wp_comment->user_id);
            if($thumbnail != null && $thumbnail['ID']) {
                $comment->thumbnail = create_image_object_by_id($thumbnail['ID']);
            }
        }
    }

    return $comment;
}

function collect_comments($postId) {

    global $wpdb;
    return $wpdb->get_results($wpdb->prepare("
      SELECT *
      FROM $wpdb->comments
      WHERE comment_post_ID = %d
        AND comment_approved = 1
        AND comment_type = ''
      ORDER BY comment_date
    ", $postId));
}

function get_last_comment_date($comments) {

    if ($comments) {
        return $comments[count($comments)-1]->comment_date;
    } else {
        return null;
    }
}

function nest_comments($comments) {

    $threads = array();
    foreach ($comments as $comment) {

        $comment = create_comment($comment);

        if ($comment->parent == 0) {
            // create thread
            $threads[$comment->id] = array($comment);
        }  else {
            // if has comment_parent push to thread of comment_parent
            if ($comment->parent && isset($threads[$comment->parent]) && is_array($threads[$comment->parent])) {
                array_push($threads[$comment->parent],$comment);
            }
        }
    }
    // reverse chronological order of threads - we need latest comments up
    $threads = array_reverse($threads);
    // reverse order of comments in thread (alleen voor zuidas)
//    foreach ($threads as $key => $thread) {
//
//        $thread_parent = $thread[0];
//
//        if(count($thread) > 1) {
//            $thread_children = array_slice($thread, 1);
//            $reversed_children = array_reverse($thread_children);
//            array_unshift($reversed_children, $thread_parent);
//            $threads[$key] = $reversed_children;
//        }
//    }
    return $threads;
}

function set_appreciation($postID) {

    $appreciation = new stdClass();

    if(get_field('positive-count', $postID)) {

        $positive_count = get_field('positive-count', $postID);
        $negative_count = get_field('negative-count', $postID);
        $total_count = $positive_count + $negative_count;
        if ($total_count > 0) {
            $percentage = round(($positive_count / $total_count) * 100);
        } else {
            $percentage = 0;
        }
        $appreciation->positive_count = $positive_count || 0;
        $appreciation->negative_count = $negative_count || 0;
        $appreciation->total_count = $total_count || 0;
        $appreciation->percentage = $percentage || 0;

        return $appreciation;

    } else {

        $appreciation->positive_count = '0';
        $appreciation->negative_count = '0';
        $appreciation->total_count = '0';
        $appreciation->percentage = '0';

        return $appreciation;
    }
}

function get_interaction($object, $field_name, $request) {

    $comments = collect_comments($object['id']);
    if($comments) {
        $nested_comments = nest_comments($comments);
    } else {
        $nested_comments = null;
    }

    $comments_count = wp_count_comments($object['id']);

    if ($comments_count->total_comments == false) {
        $comments_count->total_comments = 0;
    }

    return array(
        'appreciation' => set_appreciation($object['id']),
        'comment_count' => $comments_count->total_comments,
        'last_comment_date' => get_last_comment_date($comments),
        'nested_comments' => wph_object_to_array($nested_comments)
    );
}