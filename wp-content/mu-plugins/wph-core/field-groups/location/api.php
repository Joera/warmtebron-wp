<?php

add_action( 'init', 'register_location_field' );

    function register_location_field() {
        register_rest_field( json_decode(WPH_CONFIG)->field_groups->location,
            'location',
            array(
                'get_callback'    => 'get_location',
                'update_callback' => null,
                'schema'          => null,
            )
        );
    }

        function get_location( $object, $field_name, $request ){

            $location = get_field('location',$object['id']);


            if($location != '') {
                return $location;
            } else {
                return null;
            }
        }
