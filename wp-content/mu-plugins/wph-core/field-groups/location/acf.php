<?php 

	function my_acf_google_map_api( $api ){
		
		$api['key'] = 'AIzaSyBydTSxNfvkw3ZvGoVRIBejSSDg5ZjV4lY';
		return $api;	
	}
	add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_locatie',
			'title' => 'Locatie',
			'fields' => array (
				array (
					'key' => 'field_58f204a42aa64',
					'label' => 'Locatie',
					'name' => 'location',
					'type' => 'google_map',
					'center_lat' => '52.33818',
					'center_lng' => '4.8712359',
					'zoom' => '',
					'height' => '',
				),
			),
			'location' => configToLocations('location'),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}