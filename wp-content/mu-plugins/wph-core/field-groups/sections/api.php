<?php

add_action( 'init', 'register_sections' );

function register_sections() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->sections,
        'sections',
        array(
            'get_callback'    => 'get_sections',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_sections( $object, $field_name, $request ) {

    $sections = new stdClass;
    $sectionOrder = 0; // order of the section
    $previous = '';

    if (have_rows('section',$object['id'])): // check if the flexible content field has rows of data
        while ( have_rows('section', $object['id']) ) : the_row();
    // loop through the rows of data
            $sections->$sectionOrder = new stdClass;
            $sections->$sectionOrder->type = get_row_layout();
            $current = get_row_layout();
            // some sections require separate class when preceded by a paticular section. Mainly for setting margins.
            if (($current == 'paragraph' || $current == 'paragraph-plus') && ($previous == 'paragraph' || $previous == 'paragraph-plus')) { $sections->$sectionOrder->style = 'no-margin'; }

            if( get_row_layout() == 'paragraph' ):

                $sections->$sectionOrder->text = get_sub_field('text');

            elseif( get_row_layout() == 'chapter' ):

                $sections->$sectionOrder->title = get_sub_field('name');
                $slug = sanitize_title( get_sub_field('name') );
                $sections->$sectionOrder->slug = $slug;

//            elseif( get_row_layout() == 'image' ):
//
//                $imageType = get_sub_field('image-type');
//                $image = $imageType['image'];
//
//                $this->$sectionOrder->image = new stdClass;
//                $this->$sectionOrder->image->type = $imageType['image_type'];
//                $this->$sectionOrder->image->external_id = get_post_meta( $image['id'], 'external_id', true);
//                $this->$sectionOrder->image->filename = basename(get_attached_file( $image['id'] ));
//                $this->$sectionOrder->image->alt_text = $imageType['alt-text'];
//                $this->$sectionOrder->image->caption = $imageType['caption'];
//                $this->$sectionOrder->image->credits = $imageType['credits'];

            elseif( get_row_layout() == 'images' ) :

                $imageOrder = 0;
                $sections->$sectionOrder->images = [];
                while ( have_rows('images', $object['id']) ) : the_row();
                    $image = get_sub_field('image_clone');
                    $sections->$sectionOrder->images[$imageOrder] = create_image_object($image);
                    $imageOrder++;
                endwhile;
                if ($imageOrder == 1) {
                    $formation ='single';
                } else if ($imageOrder == 2) {
                    $formation ='duo';
                } else if ($imageOrder == 3) {
                    $formation ='trio';
                }
                $sections->$sectionOrder->formation = $formation;
                $sections->$sectionOrder->orientation = get_sub_field('orientation');

            elseif( get_row_layout() == 'quote' ):

                // $this->$sectionOrder->thumb = new stdClass;
                // $this->$sectionOrder->thumb->external_id = get_post_meta(get_sub_field('thumb')['id'], 'external_id', true);
                // $this->$sectionOrder->thumb->filename = basename(get_attached_file(get_sub_field('thumb')['id']));
                $sections->$sectionOrder->text = get_sub_field('text');
                $sections->$sectionOrder->name = get_sub_field('name');
            // $this->$sectionOrder->organisation = get_sub_field('organisation');

            elseif( get_row_layout() == 'video' ):

                if(get_sub_field('youtube-id') && get_sub_field('youtube-id') != '') :

                    $sections->$sectionOrder->host = 'youtube';
                    $sections->$sectionOrder->youtube_id = get_sub_field('youtube-id');

                elseif (get_sub_field('video_self_hosted') && get_sub_field('video_self_hosted') != '') :

                    $sections->$sectionOrder->host = 'self';
                    $sections->$sectionOrder->filename = get_sub_field('video_self_hosted');

                endif;

            elseif( get_row_layout() == 'documents' ):

                $sections->$sectionOrder->alternative_title =  get_sub_field('title');
                $sections->$sectionOrder->view =  get_sub_field('view');
                $documents = [];
                if(have_rows('files')) :
                    while ( have_rows('files') ) : the_row();
                        $fileObject = get_sub_field('document_clone');
                        $document = create_document_object($fileObject,$object);
                        if(isset($document['file_cdn_url']) && $document['file_cdn_url'] != null && $document['file_cdn_url'] != '') {
                            array_push($documents, $document);
                        }
                    endwhile;
                endif;
                $sections->$sectionOrder->documents = $documents;

            elseif ( get_row_layout() == 'streamer' ):

                $sections->$sectionOrder->text = get_sub_field('text');

            elseif ( get_row_layout() == 'planning' ):

                $sections->$sectionOrder->planning = get_sub_field('planning_select');

            endif;

//            add_filter('extra_sections','add_sections_to_api',10,3);
    //            $sections = apply_filters( 'extra_sections', $sections, get_row_layout(), $sectionOrder);

            $previous = $current;
            $sectionOrder++;

        endwhile;

            return $sections;
    else : return false;
    endif;
}
