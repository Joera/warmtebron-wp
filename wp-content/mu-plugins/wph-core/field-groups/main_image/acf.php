<?php

if(function_exists("register_field_group")) {


	register_field_group(array (
		'id' => 'acf_main_image',
		'title' => 'Hoofdafbeelding',
		'fields' => array (
			array(
				'key' => 'acf_main_image',
				'label' => '',
				'name' => 'main_image',
				'type' => 'clone',
				'instructions' => '',
				'required' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'clone' => array(
					0 => 'image_element',
				),
				'display' => 'seamless',
				'layout' => 'block',
				'prefix_label' => 1,
				'prefix_name' => 1,
			),
            array(
                'key' => 'image_tab_3',
                'label' => 'Thumb',
                'name' => '',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'placement' => 'top',
                'endpoint' => 0,
            ),
            array(

                'key' => 'image_thumb',
                'label' => 'Thumbnail',
                'name' => 'image_thumb',
                'type' => 'focal_point',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'preview_size' => 'small',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
            ),
            array(
                'key' => 'image_tab_4',
                'label' => 'Uitlijning',
                'name' => '',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'placement' => 'top',
                'endpoint' => 0,
            ),
            array (
                'key' => 'image_alignment',
                'label' => 'Uitlijning',
                'name' => 'image_alignment',
                'type' => 'radio',
                'choices' => array (
                    'top' => 'Bovenkant',
                    'center' => 'Gecentreerd',
                    'bottom' => 'Onderkant'
                ),
                'default_value' => 'center',
                'layout' => 'horizontal'
            ),


        ),

		'location' => configToLocations('main_image'),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'featured_image',
			),
		),
		'menu_order' => 0,
	));
}
