<?php

add_action( 'init', 'register_main_image' );
//add_action( 'init', 'register_square_image' ); // alternative image if automatic crop is not good enough



function register_main_image($types) {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->main_image,
        'main_image',
        array(
            'get_callback'    => 'get_main_image',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_main_image( $object, $field_name, $request ) {

    $imageObject = new stdClass();

    $main_image = get_field('main_image',$object['id']);



    if( $main_image && $main_image['image'] && $main_image['image']['ID']) {

        $imageObject = create_image_object_by_id($main_image['image']['ID']);
        $imageObject->caption = get_field('main_image_caption',$object['id']);
        $imageObject->alttext = get_field('main_image_alt-text',$object['id']);
        $imageObject->credits = get_field('main_image_credits',$object['id']);
//        if (get_field('image_thumb',$object['id']) ) {
//
//            $thumb = get_field('image_thumb',$object['id']);
//            PC::debug($thumb);
//            $imageObject->thumb = create_image_object_by_id($thumb['image']['ID']);
//        }

        $imageObject->alignment = get_field('image_alignment',$object['id']);
    }

    if ($main_image) {
        return $imageObject;
    } else {
        return false;
    }
}