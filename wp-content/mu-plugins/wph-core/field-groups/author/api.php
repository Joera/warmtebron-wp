<?php

add_action( 'init', 'register_authors' );

function register_authors() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->author,
        'author',
        array(
            'get_callback'    => 'get_author',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_author( $object, $field_name, $request ) {

    $author = new stdClass;
    $author->id = $object['author'];
    $author->first_name = get_the_author_meta('first_name', $object['author']);
    $author->last_name = get_the_author_meta('last_name', $object['author']);
    $author->organisation = get_field('organisation',"user_" . $object['author']);
    $author->bio = get_field("author_bio","user_" . $object['author']);
    $author->thumbnail = create_image_object_by_id(get_field('thumbnail',"user_" . $object['author'])['ID']);
    $author->twitter = get_field("author_twitter","user_" . $object['author']);
    $author->linkedin = get_field("author_linkedin","user_" . $object['author']);

    return $author;
}
