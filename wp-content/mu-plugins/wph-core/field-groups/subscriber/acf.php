<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_subscriber_details',
        'title' => 'Extra ',
        'fields' => array (
            array (
                'key' => 'field_subscriber_email',
                'label' => 'Email',
                'name' => 'email',
                'type' => 'email',
                'placeholder' => 'Email adres',
                'required' => 1,
                'prepend' => '',
                'append' => '',
            ),
            array (
                'key' => 'field_subscriber_subscription',
                'label' => 'Voorkeuren',
                'name' => 'subscription',
                'type' => 'select',
                'choices' => array(
                    'none'	=> 'Niet',
                    'direct'	=> 'Bij ieder nieuw artikel',
                    'weekly'	=> 'Wekelijks',
                    'monthly'	=> 'Maandelijks'
                ),
            ),
            array (
                'key' => 'field_subscriber_token',
                'label' => 'Token',
                'name' => 'token',
                'type' => 'text',
            ),
            array (
                'key' => 'field_subscriber_language',
                'label' => 'Taal',
                'name' => 'language',
                'type' => 'select',
                'choices' => array(
                    'nl'	=> 'nl',
                    'en'	=> 'en'
                ),
            )
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'subscriber'
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}