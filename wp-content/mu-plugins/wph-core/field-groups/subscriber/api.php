<?php

add_action( 'init', 'register_subscriber_fields' );

function register_subscriber_fields() {
    register_rest_field( json_decode(WPH_CONFIG)->field_groups->subscriber,
        'subscription',
        array(
            'get_callback'    => 'get_subscriber_fields',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_subscriber_fields( $object, $field_name, $request ) {

    $taxonomies = ['category','construction-project'];
    $taxonomiesTerms = [];

    foreach ($taxonomies as &$tax) {
        $terms = wp_get_post_terms($object['id'], $tax);
        foreach ($terms as &$term) {
            $taxonomiesTerms[] = $term;
        }
    }

    return array(
        'email' => get_field('email',$object['id']),
        'type' => get_field('subscription',$object['id']),
        'taxonomies' => $taxonomiesTerms,
        'language' => get_field('language',$object['id'])
    );
}