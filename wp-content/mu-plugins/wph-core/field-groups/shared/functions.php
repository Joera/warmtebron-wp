<?php

function create_image_object ($image) {

    if ( isset($image) ) {

        $imageObj = null;
        $imageObj = new stdClass;
        $imageObj->external_id = get_post_meta($image['image']['ID'], 'external_id', true);
        $imageObj->filename = basename(get_attached_file($image['image']['ID']));
        $imageObj->alttext = $image['alt-text'];
        $imageObj->caption = $image['caption'];
        $imageObj->credits = $image['credits'];
        $imageObj->wp_url = wp_get_attachment_url($image['image']['ID']);

        return $imageObj;
    };
}

function create_document_object ($document,$post) {

    if ( isset($document) ) {
        $filesize = filesize(get_attached_file($document['file']['ID']));
        $filesize = size_format($filesize, 2);
        $docObj= [];
        $docObj['post'] = $post;
        $docObj['file_id'] = $document['file']['ID'];
        $docObj['file_name'] = $document['file-name'];
        $docObj['file_cdn_id'] = get_post_meta($document['file']['ID'], 'external_id', true);
        $docObj['file_cdn_url'] = 'https://ucarecdn.com/' . get_post_meta($document['file']['ID'], 'external_id', true) . '/' . urlencode($document['file-name'] . '.pdf');
        $docObj['file_size'] = $filesize;
        $docObj['file_description'] = $document['file-description'];
        $docObj['file_tags'] = $document['file-tags'];
        return $docObj;
    };
}


function create_image_object_by_id ($id) {

    if ( isset($id) ) {
        $imageObj = null;
        $imageObj = new stdClass;
        $imageObj->external_id = new stdClass;
        $imageObj->filename = new stdClass;
        $imageObj->external_id = get_post_meta($id, 'external_id', true);
        $imageObj->filename = basename(get_attached_file($id));
        $imageObj->wp_url = wp_get_attachment_url($id);
        return $imageObj;
    };
}

function wph_object_to_array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = wph_object_to_array($value);
        }
        return $result;
    }
    return $data;
}

function configToLocations($field_group) {

    $locations = array();

    foreach (json_decode(WPH_CONFIG)->field_groups->$field_group as &$type) {
        $locations[] = array (
            array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => $type,
                'order_no' => 0,
                'group_no' => 0,
            ),
        );
    }

    return $locations;
}