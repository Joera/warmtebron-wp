<?php
function remove_properties( $data, $post, $request ) {
    $_data = $data->data;
    unset($_data['format']);
    unset($_data['template']);
    unset($_data['meta']);
    unset($_data['featured_media']);
    unset($_data['comment_status']);
    unset($_data['comment_status']);
    unset($_data['ping_status']);
    unset($_data['sticky']);
    unset($_data['categories']);
    unset($_data['tags']);


    $data->data = $_data;
    return $data;
}
add_filter( 'rest_prepare_post', 'remove_properties', 10, 3 );