<?php
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'document_element',
        'title' => 'Enkel document',
        'fields' => array(
            array(
                'key' => 'document_tab_1',
                'label' => 'Document',
                'name' => '',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'placement' => 'top',
                'endpoint' => 0,
            ),
            array (
                'key' => 'acf-file',
                'label' => 'Bestand',
                'name' => 'file',
                'type' => 'file',
                'column_width' => '',
                'save_format' => 'object',
                'library' => 'all',
            ),
            array(
                'key' => 'document_tab_2',
                'label' => 'Info',
                'name' => '',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'placement' => 'top',
                'endpoint' => 0,
            ),
            array (
                'key' => 'acf-file-name',
                'label' => 'Naam',
                'name' => 'file-name',
                'type' => 'text',
            ),
            array (
                'key' => 'acf-file-description',
                'label' => 'Beschrijving',
                'name' => 'file-description',
                'type' => 'textarea',
                'column_width' => '',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'no',
            ),
            array (
                'key' => 'acf-file-tags',
                'label' => 'Tags',
                'name' => 'file-tags',
                'type' => 'taxonomy',
                'column_width' => '',
                'taxonomy' => 'post_tag',
                'field_type' => 'checkbox',
                'allow_null' => 0,
                'load_save_terms' => 0,
                'return_format' => 'object',
                'multiple' => 0,
            ),

        ),
        'location' => array(
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

endif;

