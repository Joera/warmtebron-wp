<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5a0ee9d41e072',
	'title' => 'Image type',
	'fields' => array(
		array(
			'key' => 'field_5a0ee9d9e29e7',
			'label' => 'Afbeeldingstype',
			'name' => 'image_type',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'image-single' => 'Gewone afbeelding',
				'image-large' => 'Grote afbeelding',
				'image-left' => 'Afbeelding links',
				'image-right' => 'Afbeelding rechts',
				'multiple-images' => 'Meerdere afbeeldingen',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5a0ee9fce29e8',
			'label' => 'Afbeelding',
			'name' => 'image',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5a0ee9d9e29e7',
						'operator' => '==',
						'value' => 'image-single',
					),
				),
				array(
					array(
						'field' => 'field_5a0ee9d9e29e7',
						'operator' => '==',
						'value' => 'image-large',
					),
				),
				array(
					array(
						'field' => 'field_5a0ee9d9e29e7',
						'operator' => '==',
						'value' => 'image-left',
					),
				),
				array(
					array(
						'field' => 'field_5a0ee9d9e29e7',
						'operator' => '==',
						'value' => 'image-right',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_5a0ecf11620c2',
			),
			'display' => 'group',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
		),
		array(
			'key' => 'field_5a0eeafb7f4aa',
			'label' => 'Meerdere afbeeldingen',
			'name' => 'multiple_images',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5a0ee9d9e29e7',
						'operator' => '==',
						'value' => 'multiple-images',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_5a0ed2864ddfb',
			),
			'display' => 'group',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
		),
	),
	'location' => array(
		array(
			// array(
			// 	'param' => 'post_type',
			// 	'operator' => '==',
			// 	'value' => 'post',
			// ),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;