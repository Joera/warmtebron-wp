<?php
require_once("../../../../wp-load.php"); // load wordpress libs to enable the wordpress functions in this document
global $json_api; // json api plugin object. maakt mogelijk om: new JSON_API_Post($post); te doen

//if (is_user_logged_in()) {

    $post_id = $_GET['id']; // get post id from url
    $post = get_post($post_id); // get post data
    $type = get_post_type($post_id); // get post type

    $controller = new WP_REST_Posts_Controller($type);
    $request = new WP_REST_Request();

    $postObject = $controller->prepare_item_for_response( $post, $request );

    // send http request to frontend api
    $ch = curl_init(API_URL_FRONTEND . '/sg-api/content/preview');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Authorization: rRn8zepZA4YBDccVPm5uDYhG8t:f6UjzYMTNJ7QCkpwdWAs4U9f6V'));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postObject->data));

    $response = curl_exec($ch);
    curl_close($ch);

    $res = json_decode($response); // decode json respons$payload
    echo $res->html; // display rendered template html from response

//} else {
//  echo 'Not available';
//}